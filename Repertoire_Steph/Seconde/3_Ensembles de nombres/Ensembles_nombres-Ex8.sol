1- Non. Par exemple, la somme d'un irrationnel et de son opposé est égale à zéro (qui est un rationnel et même un entier naturel).\\
2- Oui. Par exemple, $\sqrt{2}^2=2$.\\
3- Oui, bien sûr. Par exemple, $\sqrt{16}=4$ et $4 \in \mathbb{Z}$.
