1. Vrai : si $x$ est un entier naturel, alors son double $2 x$ le sera aussi et de même pour $2 x+1$.\\
2. Vrai : on a vu dans la question précédente que $x \in \mathbb{N}$. Or. N $\in$ donc $x \in$\\
3. Faux: si $x=2$, on a $3 x-7=-1$ qui n'est pas un entier naturel. Mais $3 x-7 \in \mathbb{Z}$\\
4. Faux: si on prend $x=1$ on a $\dfrac{x-6}{2}=-2,5$. Mais $\dfrac{x-6}{2} \in \mathbb{Q}$\\
4. Faux: si on prend $x=1$\\
6. Faux: $\sqrt{2}$ n'est pas rationnel. Démonstration prochain chapitre.
