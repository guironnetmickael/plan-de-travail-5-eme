$\sqrt{2}$ est un nombre irrationnel, donc $2 \sqrt{2}$ est aussi un nombre irrationnel.\\
Or $\sqrt{2} \times 2 \sqrt{2}=4 \in \mathbb{N}$
