1. Faux: Raisonnons par l'absurde.\\
Considérons deux nombres premiers distincts $p$ et $q$ tels $\underline{p}=k \in \mathbb{Z}$
que $q \quad$ On a alors $p=k q$. Or, $p$ est un nombre premier donc la seule manière de l'écrire comme un produit de deux nombres entiers est $1 \times p$. Donc, soit $q=1$ ce qui est absurde car 1 n'est pas premier, soit $k=1$ ce qui est absurde car alors $p=q$. Donc le quotient de deux nombres premiers distincts ne peut pas être un nombre entier naturel. De plus le quotient de deux nombres premiers est positif. Donc le quotient de deux nombres premiers distincts ne peut pas être un entier relatif.\\
2. Vrai : 2 et 5 sont des nombres premiers distincts et $\dfrac{2}{5}=\dfrac{4}{10}$ est un nombre décimal.
