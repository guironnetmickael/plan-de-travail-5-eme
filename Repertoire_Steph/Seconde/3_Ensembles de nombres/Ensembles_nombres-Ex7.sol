1. Faux: 5 et 7 sont des nombres entiers naturels mais. $5-7=-2$ n'est pas un nombre entier naturel.\\
2. Faux: $1=\dfrac{1}{10^0}$ et $3=\dfrac{3}{10^\mu}$ sont des nombres décimaux mais 3 n'est pas un nombre décimal.\\
3. Faux $: \sqrt{2}$ et 1 sont des nombres réels mais $\dfrac{\sqrt{2}}{1}-\sqrt{2}$ n'est pas un nombre rationnel.\\
4. Vrai : Soient $m=\dfrac{b}{b}$ un nombre rationnel oủ $b+0$ et $h \in \mathbb{Z}$ Par définition de $m a \in \mathbb{Z}$ et Ainsi, $h u \in \mathbb{Q}, \mathrm{et}$ ainsi : $m \times k-\dfrac{k \pi}{b} \in Q$
