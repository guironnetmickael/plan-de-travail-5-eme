$ \mathrm{I}=y^5 \quad \mathrm{~J}=x^{16} \quad \mathrm{~K}=(x y)^{14}$
$\mathrm{L}=-x^3 y^4 \quad \mathrm{M}=$ impossible à simplifier
$$
\mathrm{N}=y \quad \mathrm{O}=x^{-7} \quad \mathrm{P}=x^{-3} y^4 \quad \mathrm{Q}=0
$$
