\begin{description}
\item[\textbullet]  $8^{-3}=\dfrac{1}{8 ^3}$
\item[\textbullet] $4 ^{-4}=\dfrac{1}{4^5}$
\item[\textbullet] $9^{-3}=\dfrac{1}{9^{3}}$
\item[\textbullet] $\dfrac{1}{4^{ 3}}=4^{-3}$
\end{description}
