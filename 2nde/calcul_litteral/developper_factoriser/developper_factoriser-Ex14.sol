  \begin{enumerate}[label=\arabic*.]
    \item Pour $x\in \mathbb{R}^*$,\\
              $\begin{aligned}
              A&=4x+\dfrac{6}{x}\\
              &=\dfrac{4x^2}{x}+\dfrac{6}{x}\\
             &= \dfrac{4x^2+6}{x}
             \end{aligned}$

     \item L'équation $4x+12=0$ a pour solution $-3$. \\
              $-3$ est donc une valeur interdite pour l'expression. \\
              Pour $x\in \mathbb{R}\smallsetminus\left\{-3\right\}$, \\
              $\begin{aligned}
  B&=1-\dfrac{13}{4x+12}\\
  &=\dfrac{1(4x+12)}{4x+12}-\dfrac{13}{4x+12}\\
              &=\dfrac{4x+12-13}{4x+12}\\
              &=\dfrac{4x-1}{4x+12}
              \end{aligned}$
            \end{enumerate}
