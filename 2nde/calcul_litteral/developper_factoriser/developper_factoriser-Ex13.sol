  \begin{enumerate}[itemsep=2em,label={}]
    \item $ A= x(x+3)+4(x+3)$.\\
    On remarque que $(x+3)$ est un facteur commun.\\
    $\phantom{ABC}={\color{blue}\boldsymbol{x}}{\color[HTML]{f15929}\boldsymbol{(x+3)}}+{\color{blue}\boldsymbol{4}}{\color[HTML]{f15929}\boldsymbol{(x+3)}}$\\
    $\phantom{ABC}={\color[HTML]{f15929}\boldsymbol{(x+3)}}{\color{blue}\boldsymbol{(x+4)}}$\\
    \item $ B= x(3x+2)+5(3x+2)$\,\,On remarque que $(3x+2)$ est un facteur commun.\\
    $\phantom{ABC}={\color{blue}\boldsymbol{x}}{\color[HTML]{f15929}\boldsymbol{(3x+2)}}+{\color{blue}\boldsymbol{5}}{\color[HTML]{f15929}\boldsymbol{(3x+2)}}$\\
    $\phantom{ABC}={\color[HTML]{f15929}\boldsymbol{(3x+2)}}{\color{blue}\boldsymbol{(x+5)}}$\\
  \end{enumerate}
