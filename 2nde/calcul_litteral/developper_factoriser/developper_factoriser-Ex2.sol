  \begin{enumerate}[itemsep=1em]
    \item $=5b+4b={\color[HTML]{f15929}\boldsymbol{9b}}$
    \item $=6a+5={\color[HTML]{f15929}\boldsymbol{6a+5}}$
    \item $=7a\times9a={\color[HTML]{f15929}\boldsymbol{63a^2}}$
    \item $=8y\times4={\color[HTML]{f15929}\boldsymbol{32y}}$
  \end{enumerate}
