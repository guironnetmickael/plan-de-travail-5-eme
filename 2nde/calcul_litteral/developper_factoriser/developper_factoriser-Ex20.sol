    Pour démontrer que le produit de deux nombres qui diffèrent de \(2\) peut être calculé en prenant le carré de leur milieu et en retirant \(1\), considérons deux nombres \(x\) et \(x+2\).

Calculons le produit \(x \times (x+2)\) :

\[
x \times (x+2) = x^2 + 2x
\]

Déterminons maintenant le milieu des deux nombres \(x\) et \(x+2\) :

\[
\frac{x + (x+2)}{2} = \frac{2x + 2}{2} = x + 1
\]

Prenons le carré de ce milieu :

\[
(x + 1)^2
\]

Développons cette expression :

\[
(x + 1)^2 = x^2 + 2x + 1
\]

Ensuite, retirons \(1\) de cette expression :

\[
(x + 1)^2 - 1 = x^2 + 2x + 1 - 1 = x^2 + 2x
\]

Nous constatons que cette expression est égale à \(x \times (x+2)\) :

\[
x^2 + 2x = x \times (x+2)
\]

Ainsi, nous avons démontré que :

\[
x \times (x+2) = \left( \frac{x + (x+2)}{2} \right)^2 - 1
\]

Cela montre que le produit de deux nombres qui diffèrent de \(2\) est égal au carré de leur milieu moins \(1\).

