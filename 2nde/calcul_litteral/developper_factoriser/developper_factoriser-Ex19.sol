 En mettant en évidence une différence de deux carrés, factoriser les expressions suivantes.
 \begin{enumerate}
  \item $=(x-11)(x+11)$
  \item $ = (x - 10)(x+2)$
  \item $ = (x-\sqrt{5})(x+\sqrt{5})$
  \item $ = (3+x)(7-x) $
 \end{enumerate}
 
