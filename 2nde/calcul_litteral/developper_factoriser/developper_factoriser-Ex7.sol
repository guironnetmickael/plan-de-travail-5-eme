  \begin{enumerate}
    \item  $=\left(\dfrac{4}{7}x-4\right)\left(\dfrac{4}{7}x+4\right)=\left(\dfrac{4}{7}x\right)^2-4^2=\dfrac{16}{49}x^2-16$

    \item  $=\left(\dfrac{4}{9}x+9\right)^2=\left(\dfrac{4}{9}x\right)^2+2 \times \dfrac{4}{9}x \times 9 + 9^2=\dfrac{16}{81}x^2+\dfrac{72}{9}x+81$
  \end{enumerate}
