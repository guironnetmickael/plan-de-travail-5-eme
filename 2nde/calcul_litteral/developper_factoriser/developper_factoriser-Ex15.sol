  \begin{enumerate}[label=\arabic*.]
    \item
                  L'équation $6x-3=0$ a pour solution $\dfrac{1}{2}$. \\
                L'équation $2x+8=0$ a pour solution $-4$. \\
                $\dfrac{1}{2}$ et $-4$ sont donc des valeurs interdites pour l'expression. \\
                Pour $x\in \mathbb{R}\smallsetminus\left\{-4\,;\,\dfrac{1}{2}\right\}$, \\
                $\begin{aligned}
                \dfrac{9}{6x-3}-\dfrac{9}{2x+8}
                &= \dfrac{9(2x+8)}{(6x-3)(2x+8)}-\dfrac{9(6x-3)}{(6x-3)(2x+8)}\\
                &=\dfrac{9(2x+8)-9(6x-3)}{(6x-3)(2x+8)}\\
                &=\dfrac{-36x+99}{(6x-3)(2x+8)}
                \end{aligned}$
    \item Déterminer les valeurs interdites de cette expression, revient à déterminer les valeurs qui annulent le dénominateur de $\dfrac{3}{3x+2}$,
                puisque la division par $0$ n'existe pas.\\ L'équation $3x+2=0$ a pour solution $\dfrac{-2}{3}$. \\
                 $0$ et $\dfrac{-2}{3}$ sont donc des valeurs interdites pour l'expression. \\
                Pour $x\in \mathbb{R}\smallsetminus\left\{\dfrac{-2}{3}\,;\,0\right\}$,\\
                $\begin{aligned}
                \dfrac{4}{x}-\dfrac{3}{3x+2}&=\dfrac{4(3x+2)}{x(3x+2)}-\dfrac{3x}{x(3x+2)}\\
               & =\dfrac{12x+8-3x}{x(3x+2)}\\
               &=\dfrac{9x+8}{x(3x+2)}
               \end{aligned}$
    \end{enumerate}

