 Souligner le facteur commun puis écrire la factorisation.
 \begin{enumerate}
 \item $ = (2x-3)(2x+2)$
 \item $=(15x+7)(-13x-2)$
 \item $=3x(3x+2)$
 \item $=(13x+5)(3x-13)$
  \item $ = x(8x-3)$
 \end{enumerate}
 
