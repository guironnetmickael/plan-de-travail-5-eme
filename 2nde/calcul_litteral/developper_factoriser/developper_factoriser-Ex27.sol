 \begin{enumerate}
 \item développer l'expression.
 \item
 \begin{enumerate}
 \item canonique: $3(3-3)^2+5=5$
 \item développer: $3 \left(\sqrt{2}\right)^2-18 \sqrt{2}+32=38-18\sqrt{2}$
 \item développer: $0^2-18\times 0+32=32$
 \end{enumerate}
 \end{enumerate}
 
