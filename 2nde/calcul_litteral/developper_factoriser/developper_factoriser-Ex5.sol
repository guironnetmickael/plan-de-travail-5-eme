  \begin{enumerate}[label=\arabic*.. ]
    \item On développe l'expression en utilisant l'identité remarquable $(a+b)(a-b)=a^2-b^2$, \\
    avec $\color{red} a = x\color{black}$ et $\color{green} b = 4 \color{black} $ : \\
    $A=(x-4)(x+4)=x^2-4^2=x^2-16$
    \item On développe l'expression en utilisant l'identité remarquable $(a+b)^2=a^2+2ab+b^2$, \\
    avec $\color{red} a = x\color{black}$ et $\color{green} b = 9 \color{black} $ : \\
    $B=\left(\color{red}x\color{black}+\color{green}9\color{black}\right)^2=\color{red}x\color{black}^2+2 \times \color{red}x \color{black}\times \color{green}9 \color{black}+ \color{green}9\color{black}^2$ \\$\phantom{\left(\color{red}x\color{black}+\color{green}9\color{black}\right)^2} = x^2+18x+81$
  \end{enumerate}
