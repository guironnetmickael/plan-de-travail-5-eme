  \begin{enumerate}[itemsep=2em,label=\Alph*]
    \item $=-3-(7x+8)(9x+10)$\\$\phantom{A}=-3-(63x^2+70x+72x+80)$\\$\phantom{A}=-3-63x^2-70x-72x-80$\\$\phantom{A}=-63x^2-142x-83$
    \item $=8x+6(10x-1)$\\$\phantom{B}=8x+60x-6$\\$\phantom{B}=68x-6$
  \end{enumerate}
