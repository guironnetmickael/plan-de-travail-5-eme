
  $\begin{array}{rl}
      A&= -(c+6) + (-5c^2-3c+8)\\
      &= -c-6  -5c^2-3c+8\\
      &=\color[HTML]{f15929}\boldsymbol{-5c^2-4c+2}
    \end{array}$
    $\begin{array}{rl}
      B&= (-z^2-7z+5) - (-8z^2+z-4)\\
      &= -z^2-7z+5 + 8z^2-z+4\\
      &={\color[HTML]{f15929}\boldsymbol{7z^2-8z+9}}
     \end{array}$


