  \begin{enumerate}[label=\arabic*.]
    \item On développe l'expression en utilisant l'identité remarquable $(a+b)(a-b)=a^2-b^2$, \\
    avec $\color{red} a = 3x\color{black}$ et $\color{green} b = 4 \color{black} $ : \\
    $(3x-4)(3x+4)=(3x)^2-4^2=9x^2-16$
    \item On développe l'expression en utilisant l'identité remarquable $(a+b)^2=a^2+2ab+b^2$, \\
     avec $\color{red} a = 7x\color{black}$ et $\color{green} b = 9 \color{black} $ : \\$(7x+9)^2=(7x)^2+2 \times 7x \times 9 + 9^2=49x^2+126x+81$
  \end{enumerate}
