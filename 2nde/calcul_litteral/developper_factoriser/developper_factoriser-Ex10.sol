  \begin{enumerate}[itemsep=1em]
    \item $=2a+8b$\\
    $\phantom{A}=2a+2\times4b$\\
    $\phantom{A}=$ ${\color[HTML]{f15929}\boldsymbol{2(a+4b)}}$
    \item $=28x+49x^2$\\
    $\phantom{B}=7x\times4+7x\times7x$
    \\$\phantom{B}=$ ${\color[HTML]{f15929}\boldsymbol{7x(4+7x)}}$
  \end{enumerate}
