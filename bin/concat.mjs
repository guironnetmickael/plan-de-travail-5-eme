import { readFileSync, writeFileSync } from "node:fs"
import { join, basename } from "node:path"
import { execSync } from "node:child_process"
import { createRequire } from "node:module"

const require = createRequire(import.meta.url)
const readline = require("node:readline")
const fs = require("node:fs")
const path = require("node:path")

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

rl.question("Entrez le nom du chapitre : ", (chapitre) => {
  const dirPath = path.join("./", chapitre, "tex")
  fs.readdir(dirPath, (err, files) => {
    if (err) {
      console.error(`Impossible de lire le répertoire ${dirPath}`)
      process.exit(1)
    }

    const texFiles = files
      .filter((file) => path.extname(file) === ".tex")
      .filter((file) => !file.includes("R.tex"))

    let cpt = 1
    for (const file of texFiles) {
      const filePath = path.join(dirPath, file)
      const filePath2 = filePath.replace(".tex", "R.tex")
      console.log(`Concat ${filePath} avec ${filePath2}...`)
      const content = readFileSync(filePath, "utf8")
        .replace("\\begin{exercice*}", "\\begin{exercice}")
        .replace("\\end{exercice*}", "\\end{exercice}")
      const content2 = readFileSync(filePath2, "utf8")
        .replace(/\\begin{exercice\*?}/g, "\\begin{Solution}")
        .replace(/\\end{exercice\*?}/g, "\\end{Solution}")
      const newFilePath = path.join(
        path.dirname(filePath),
        `${chapitre}_${cpt.toString().padStart(3, "0")}.tex`
      )
      writeFileSync(newFilePath, `${content}\n\n${content2}`)
      cpt++
    }
  })

  rl.close()
})
