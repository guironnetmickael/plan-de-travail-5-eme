%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Welcome to Overleaf --- just edit your LaTeX on the left,
% and we'll compile it for you on the right. If you open the
% 'Share' menu, you can invite other users to edit at the same
% time. See www.overleaf.com/learn for more info. Enjoy!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[a4paper,11pt,fleqn]{article}

\usepackage{mathtools,amssymb,amsfonts,mathrsfs}

\usepackage{arev}

%%% COULEURS %%%
\usepackage[table,svgnames]{xcolor}
\definecolor{nombres}{cmyk}{0,.8,.95,0}
\definecolor{gestion}{cmyk}{.75,1,.11,.12}
\definecolor{gestionbis}{cmyk}{.75,1,.11,.12}
\definecolor{grandeurs}{cmyk}{.02,.44,1,0}
\definecolor{geo}{cmyk}{.62,.1,0,0}
\definecolor{algo}{cmyk}{.69,.02,.36,0}
\definecolor{correction}{cmyk}{.63,.23,.93,.06}
\arrayrulecolor{couleur_theme} % Couleur des filets des tableaux

\usepackage[autolanguage,np]{numprint}

\newcommand\degree{\degres}

\usepackage[most]{tcolorbox}
\tcbuselibrary{documentation}

%%%%%%%% Modification paramétrage pour footnotes
% Le paquet semble déjà appelé en amont, curieux qu'il n'y ait pas eu de clash
% avec l'appel ci-dessous mais seulement quand j'ai essayé de l'inclure avec l'option lualatex

% \usepackage{hyperref}
% Parametrages
\hypersetup{
    colorlinks=true,% On active la couleur pour les liens. Couleur par défaut rouge
    linkcolor=black,% On définit la couleur pour les liens internes
    % filecolor=magenta,% On définit la couleur pour les liens vers les fichiers locaux
    urlcolor=black,% On définit la couleur pour les liens vers des sites web
    % pdftitle={Puissance Quatre},% On définit un titre pour le document pdf
    % pdfpagemode=FullScreen,% On fixe l'affichage par défaut à plein écran
    }
% Pour avoir les numérotations arabic y compris dans les environnements tcolorbox
\renewcommand{\thempfootnote}{\arabic{mpfootnote}}
%%%%%%%%
\usepackage{multicol}
\usepackage{calc}

\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{tabularx}

\setlength{\parindent}{0mm}
\renewcommand{\arraystretch}{1.5}
\renewcommand{\labelenumi}{\textbf{\theenumi{}.}}
\renewcommand{\labelenumii}{\textbf{\theenumii{}.}}
\setlength{\fboxsep}{3mm}

\setlength{\headheight}{14.5pt}

\setlength{\premulticols}{0pt}

\newcounter{ExoMA}

\newlength{\parindentMA}%
\setlength{\parindentMA}{\parindent}
\addtolength{\parindentMA}{30pt}

\usepackage{simplekv}

\setKVdefault[Theme]{Coopmath,Classiques=false,CANs=false}
\defKV[Theme]{Classique=\setKV[Theme]{Coopmath=false}\setKV[Theme]{Classiques}}
\defKV[Theme]{CAN=\setKV[Theme]{Coopmath=false}\setKV[Theme]{CANs}}

\usepackage{fancyhdr}
\fancyhead[C]{}
\fancyfoot{}

\def\bla{}

\NewDocumentCommand\Theme{o m m m m}{%
  \useKVdefault[Theme]%
  \setKV[Theme]{#1}%
  \ifboolKV[Theme]{CANs}{%
    \usepackage[left=1.5cm,right=1.5cm,top=2cm,bottom=2cm]{geometry}%
    \fancypagestyle{premierePage}{%
      \fancyhead[C]{\textsc{\ifx\bla#2\bla Course aux nombres\else#2\fi}}%
      \fancyfoot[R]{\scriptsize Coopmaths.fr -- CC-BY-SA}%
      \setlength{\headheight}{14.5pt}%
      \NewDocumentCommand\dureeCan{}{\ifx\bla#4\bla 9 minutes\else #4\fi}
      \NewDocumentCommand\titreSujetCan{}{\ifx\bla#3\bla\else\textbf{#3}\fi}
    }%
    \pagestyle{premierePage}
    \colorlet{couleur_theme}{black}%
    \colorlet{couleur_numerotation}{couleur_theme}%
    \let\EXO\EXOCan\let\endEXO\endEXOCan%
  }{%
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}
    \pagestyle{fancy}
    \ifboolKV[Theme]{Classiques}{%
      \usepackage[left=0.65cm,right=0.65cm,top=2cm,bottom=1.5cm]{geometry}
      \let\EXO\EXOlibre\let\endEXO\endEXOlibre%
      \colorlet{couleur_theme}{black}%
      \colorlet{couleur_numerotation}{couleur_theme}%
      \fancyhead[C]{\bfseries #3}
      \fancyhead[L]{\bfseries #4}
      \fancyfoot[R]{#5}
    }{%
      \usepackage[left=1.5cm,right=1.5cm,top=4cm,bottom=2cm]{geometry}
      \theme{#2}{#3}{#4}{#5}%
      \let\EXO\EXOcoop\let\endEXO\endEXOcoop%
    }%
  }%
}%

\NewDocumentEnvironment{EXOCan}{m m +b}{%
  #3
}

\NewDocumentEnvironment{EXOcoop}{m m +b}{%
  \begin{tcolorbox}[%
    enhanced,
    breakable,
    colback=white,
    colframe=white,
    coltitle=black,
    title=\IfNoValueTF{#1}{}{\textmd{#1}},%
    overlay unbroken and first={%
      \IfNoValueTF{#2}{}{%
        \node[%
        fill=white,
        anchor=east,
        xshift=10pt,
        text=black
        ]
        at (frame.north east)
        {\scriptsize #2};
      }
      \node[anchor=west,xshift=-20pt,yshift=-15pt] at (frame.north west) {\stepcounter{ExoMA}\numb{\arabic{ExoMA}}};
    }
    ]
    #3
  \end{tcolorbox}
}

\NewDocumentEnvironment{EXOlibre}{m m +b}{%
  \begin{tcolorbox}[%
    enhanced,
    breakable,
    colback=white,
    colframe=white,%couleur_theme,
    coltitle=black,
    title=\stepcounter{ExoMA}\textbf{Exercice \arabic{ExoMA}},%
    ]
    \IfNoValueTF{#1}{}{#1\par}%
    #3
  \end{tcolorbox}%
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SPÉCIFIQUE SUJETS CAN                  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{longtable}

\tikzset{
  mybox/.style={
    rectangle,
    drop shadow,
    inner sep=17pt,
    draw=gray,
    shade,
    top color=gray,
    every shadow/.append style={fill=gray!40},
    bottom color=gray!20
    }
  }

  \newcommand\MyBox[2][]{%
    \tikz\node[mybox,#1] {#2};
  }
  % Un compteur pour les questions CAN
  \newcounter{nbEx}
  % Pour travailler avec les compteurs
  \usepackage{totcount}
  \regtotcounter{nbEx}

  % Une checkmark !
  \def\myCheckmark{\tikz\fill[scale=0.4](0,.35) -- (.25,0) -- (1,.7) -- (.25,.15) -- cycle;}
  % Repiqué sans vergogne dans lemanuel TikZ pour l'impatient
  \def\arete{3}   \def\epaisseur{5}   \def\rayon{2}

  \newcommand{\ruban}{(0,0)
    ++(0:0.57735*\arete-0.57735*\epaisseur+2*\rayon)
    ++(-30:\epaisseur-1.73205*\rayon)
    arc (60:0:\rayon)   -- ++(90:\epaisseur)
    arc (0:60:\rayon)   -- ++(150:\arete)
    arc (60:120:\rayon) -- ++(210:\epaisseur)
    arc (120:60:\rayon) -- cycle}

  \newcommand{\mobiusCan}{
    % Repiqué sans vergogne dans lemanuel TikZ pour l'impatient
    \begin{tikzpicture}[very thick,top color=white,bottom color=gray,scale=1.2]
      \shadedraw \ruban;
      \shadedraw [rotate=120] \ruban;
      \shadedraw [rotate=-120] \ruban;
      \draw (-60:4) node[scale=5,rotate=30]{CAN};
      \draw (180:4) node[scale=3,rotate=-90]{MathALEA};
      \clip (0,-6) rectangle (6,6); % pour croiser
      \shadedraw  \ruban;
      \draw (60:4) node [gray,xscale=2.5,yscale=2.5,rotate=-30]{CoopMaths};
    \end{tikzpicture}
  }

  \newcommand{\pageDeGardeCan}[1]{
    % #1 --> nom du compteur pour le nombre de questions

    %\vspace*{10mm}
    \textsc{Nom} : \makebox[.35\linewidth]{\dotfill} \hfill \textsc{Prénom} : \makebox[.35\linewidth]{\dotfill}

    \vspace{10mm}
    \textsc{Classe} : \makebox[.33\linewidth]{\dotfill} \hfill
    \MyBox{\Large\textsc{Score} : \makebox[.15\linewidth]{\dotfill} / \total{#1}}
    \par\medskip \hrulefill \par
    \myCheckmark \textit{\textbf{Durée :  \dureeCan}}

    \smallskip
    \myCheckmark \textit{L'épreuve comporte \total{#1} questions.}

    \smallskip
    \myCheckmark \textit{L'usage de la calculatrice et du brouillon est interdit.}

    \smallskip
    \myCheckmark \textit{Il n'est pas permis d'écrire des calculs intermédiaires.}
    \par \hrulefill \par\vspace{5mm}
    \begin{center}
      \textsc{\titreSujetCan}
      \par\vspace{5mm}
      \mobiusCan
    \end{center}
  }

  \newlength{\Largeurcp}

  % Structure globale pour les tableaux des livrets CAN
  \NewDocumentEnvironment{TableauCan}{b}{%
    % #1 --> corps de tableau
    \setlength{\Largeurcp}{0.35\textwidth-8\tabcolsep}
    \renewcommand*{\arraystretch}{2.5}
    \begin{spacing}{1.1}
      \begin{longtable}{|>{\columncolor{gray!20}\centering}m{0.05\textwidth}|>{\centering}m{0.45\textwidth}|>{\centering}m{\Largeurcp}|>{\centering}p{0.1\textwidth}|}%
        \hline
        \rowcolor{gray!20}\#&Énoncé&Réponse&Jury\tabularnewline \hline
        % \endfirsthead
        % \hline
        % \rowcolor{gray!20}\#&Énoncé&Réponse&Jury\tabularnewline \hline
        % \endhead
        #1
      \end{longtable}
    \end{spacing}
    \renewcommand*{\arraystretch}{1}
  }{}

%%% MISE EN PAGE %%%

\usepackage{setspace}

\usepackage{tkz-tab,tkz-fct}
\usepackage{tkz-euclide}
\usepackage{pgf}%
\usepackage{pgfplots}
\pgfplotsset{compat=1.18}
\usetikzlibrary{babel,arrows,calc,fit,patterns,plotmarks,shapes.geometric,shapes.misc,shapes.symbols,shapes.arrows,shapes.callouts, shapes.multipart, shapes.gates.logic.US,shapes.gates.logic.IEC, er, automata,backgrounds,chains,topaths,trees,petri,mindmap,matrix, calendar, folding,fadings,through,positioning,scopes,decorations.fractals,decorations.shapes,decorations.text,decorations.pathmorphing,decorations.pathreplacing,decorations.footprints,decorations.markings,shadows}

\spaceskip=2\fontdimen2\font plus 3\fontdimen3\font minus3\fontdimen4\font\relax %Pour doubler l'espace entre les mots
\newcommand\numb[1]{ % Dessin autour du numéro d'exercice
  \begin{tikzpicture}[overlay,scale=.8]%,yshift=-.3cm
    \draw[fill=couleur_numerotation,couleur_numerotation](-.3,0)rectangle(.5,.8);
    \draw[line width=.05cm,couleur_numerotation,fill=white] (0,0)--(.5,.5)--(1,0)--(.5,-.5)--cycle;
    \draw (0.5,0) node[anchor=center,couleur_numerotation]{\large\bfseries#1};
    \draw (-.4,.8) node[white,anchor=north west]{\bfseries EX};
  \end{tikzpicture}
}

% echelle pour le dé
\def\globalscale {0.04}
% abscisse initiale pour les chevrons
\def\xini {3}

\newcommand\theme[4]{%
  \fancyhead[C]{%
    % Tracé du dé
    \begin{tikzpicture}[y=0.80pt, x=0.80pt, yscale=-\globalscale, xscale=\globalscale,remember picture, overlay,transform canvas={xshift=-0.5\linewidth,yshift=2.7cm},fill=couleur_theme]%,xshift=17cm,yshift=9.5cm
      %%%% Arc supérieur gauche%%%%
      \path[fill](523,1424)..controls(474,1413)and(404,1372)..(362,1333)..controls(322,1295)and(313,1272)..(331,1254)..controls(348,1236)and(369,1245)..(410,1283)..controls(458,1328)and(517,1356)..(575,1362)..controls(635,1368)and(646,1375)..(643,1404)..controls(641,1428)and(641,1428)..(596,1430)..controls(571,1431)and(538,1428)..(523,1424)--cycle;
      %%%% Dé face supérieur%%%%
      \path[fill](512,1272)..controls(490,1260)and(195,878)..(195,861)..controls(195,854)and(198,846)..(202,843)..controls(210,838)and(677,772)..(707,772)..controls(720,772)and(737,781)..(753,796)..controls(792,833)and(1057,1179)..(1057,1193)..controls(1057,1200)and(1053,1209)..(1048,1212)..controls(1038,1220)and(590,1283)..(551,1282)..controls(539,1282)and(521,1278)..(512,1272)--cycle;
      %%%% Dé faces gauche et droite%%%%
      \path[fill](1061,1167)..controls(1050,1158)and(978,1068)..(900,967)..controls(792,829)and(756,777)..(753,756)--(748,729)--(724,745)..controls(704,759)and(660,767)..(456,794)..controls(322,813)and(207,825)..(200,822)..controls(193,820)and(187,812)..(187,804)..controls(188,797)and(229,688)..(279,563)..controls(349,390)and(376,331)..(391,320)..controls(406,309)and(462,299)..(649,273)..controls(780,254)and(897,240)..(907,241)..controls(918,243)and(927,249)..(928,256)..controls(930,264)and(912,315)..(889,372)..controls(866,429)and(848,476)..(849,477)..controls(851,479)and(872,432)..(897,373)..controls(936,276)and(942,266)..(960,266)..controls(975,266)and(999,292)..(1089,408)..controls(1281,654)and(1290,666)..(1290,691)..controls(1290,720)and(1104,1175)..(1090,1180)..controls(1085,1182)and (1071,1176)..(1061,1167)--cycle;
      %%%% Arc inférieur bas%%%%
      \path[fill](1329,861)..controls(1316,848)and(1317,844)..(1339,788)..controls(1364,726)and(1367,654)..(1347,591)..controls(1330,539)and(1338,522)..(1375,526)..controls(1395,528)and(1400,533)..(1412,566)..controls(1432,624)and(1426,760)..(1401,821)..controls(1386,861)and(1380,866)..(1361,868)..controls(1348,870)and(1334,866)..(1329,861)--cycle;
      %%%% Arc inférieur gauche%%%%
      \path[fill](196,373)..controls(181,358)and(186,335)..(213,294)..controls(252,237)and(304,190)..(363,161)..controls(435,124)and(472,127)..(472,170)..controls(472,183)and(462,192)..(414,213)..controls(350,243)and(303,283)..(264,343)..controls(239,383)and(216,393)..(196,373)--cycle;
    \end{tikzpicture}
    \begin{tikzpicture}[remember picture,overlay]
      \node[anchor=north east,inner sep=0pt] at ($(current page.north east)+(0,-.8cm)$) {};
      \node[anchor=east, fill=white] at ($(current page.north east)+(-18.8,-2.3cm)$) {\footnotesize \bfseries{MathALEA}};
    \end{tikzpicture}
    \begin{tikzpicture}[line cap=round,line join=round,remember picture, overlay, shift={(current page.north west)},yshift=-8.5cm]
      \fill[fill=couleur_theme] (0,5) rectangle (21,6);
      \fill[fill=couleur_theme] (\xini,6)--(\xini+1.5,6)--(\xini+2.5,7)--(\xini+1.5,8)--(\xini,8)--(\xini+1,7)-- cycle;
      \fill[fill=couleur_theme] (\xini+2,6)--(\xini+2.5,6)--(\xini+3.5,7)--(\xini+2.5,8)--(\xini+2,8)--(\xini+3,7)-- cycle;
      \fill[fill=couleur_theme] (\xini+3,6)--(\xini+3.5,6)--(\xini+4.5,7)--(\xini+3.5,8)--(\xini+3,8)--(\xini+4,7)-- cycle;
      \node[color=white] at (10.5,5.5) {\LARGE \bfseries{ \MakeUppercase{#4}}};
    \end{tikzpicture}
    \begin{tikzpicture}[remember picture,overlay]
      \node[anchor=north east,inner sep=0pt] at ($(current page.north east)+(0,-.8cm)$) {};
      \node[anchor=east, fill=white] at ($(current page.north east)+(-2,-1.5cm)$) {\Huge \textcolor{couleur_theme}{\bfseries{\#}} \bfseries{#2} \textcolor{couleur_theme}{\bfseries \MakeUppercase{#3}}};
    \end{tikzpicture}
  }%
  \fancyfoot[R]{%
    \begin{tikzpicture}[remember picture,overlay]
      \node[anchor=south east] at ($(current page.south east)+(-2,0.25cm)$) {\scriptsize {\bfseries \href{https://coopmaths.fr/}{Coopmaths.fr} -- \href{http://creativecommons.fr/licences/}{CC-BY-SA}}};
    \end{tikzpicture}
    \begin{tikzpicture}[line cap=round,line join=round,remember picture, overlay,xscale=0.5,yscale=0.5, shift={(current page.south west)},xshift=35.7cm,yshift=-6cm]
      \fill[fill=couleur_theme] (\xini,6)--(\xini+1.5,6)--(\xini+2.5,7)--(\xini+1.5,8)--(\xini,8)--(\xini+1,7)-- cycle;
      \fill[fill=couleur_theme] (\xini+2,6)--(\xini+2.5,6)--(\xini+3.5,7)--(\xini+2.5,8)--(\xini+2,8)--(\xini+3,7)-- cycle;
      \fill[fill=couleur_theme] (\xini+3,6)--(\xini+3.5,6)--(\xini+4.5,7)--(\xini+3.5,8)--(\xini+3,8)--(\xini+4,7)-- cycle;
    \end{tikzpicture}
  }
  \fancyfoot[C]{}
  \colorlet{couleur_theme}{#1}
  \colorlet{couleur_numerotation}{couleur_theme}
  \def\iconeobjectif{icone-objectif-#1}
  \def\urliconeomethode{icone-methode-#1}
}%

%%%%%%% NOTATIONS DES ENSEMBLES %%%%%%%
\newcommand\R{\mathbb{R}}
\newcommand\N{\mathbb{N}}
\newcommand\D{\mathbb{D}}
\newcommand\Z{\mathbb{Z}}
\newcommand\Q{\mathbb{Q}}

\newcommand*\tikzfootMA{%
  \ifboolKV[Theme]{CANs}{
    \begin{tikzpicture}[remember picture,overlay,shift=(current page.north west)]
      \begin{scope}[x=\textwidth-\oddsidemargin,y=\textheight+5pt]
        \draw[correction,line width=4pt,dashed,dash pattern= on 10pt off 10pt,shift={(-\oddsidemargin,\topmargin)}] (0,0) rectangle (1,-1);
      \end{scope}
    \end{tikzpicture}
  }{%
  \ifboolKV[Theme]{Classiques}{%
  \begin{tikzpicture}[remember picture,overlay,shift=(current page.south west)]
    \begin{scope}[x={(current page.south east)},y={(current page.north west)}]
      \draw[correction,line width=4pt,dashed,dash pattern= on 10pt off 10pt] ($(0,0)+(5mm,1cm)$) rectangle ($(1,1)+(-5mm,-1.75cm)$);
    \end{scope}
  \end{tikzpicture}
  }{%
  \begin{tikzpicture}[remember picture,overlay,shift=(current page.south west)]
    \begin{scope}[x={(current page.south east)},y={(current page.north west)}]
      \draw[correction,line width=4pt,dashed,dash pattern= on 10pt off 10pt] ($(0,0)+(5mm,1.5cm)$) rectangle ($(1,1)+(-5mm,-3.75cm)$);
    \end{scope}
  \end{tikzpicture}
  }%
  }
}

\newenvironment{Correction}{%
  \setcounter{ExoMA}{0}%
  \cfoot{\tikzfootMA}
}{}%

\newcommand\version[1]{
  \fancyhead[R]{
    \begin{tikzpicture}[remember picture,overlay]
    \node[anchor=north east,inner sep=0pt] at ($(current page.north east)+(-.5,-.5cm)$) {\large \textcolor{couleur_theme}{\bfseries V#1}};
    \end{tikzpicture}
  }
}

\newcommand\dotfills[1][4cm]{\makebox[#1]{\dotfill}}

%%%%% NOMBRES PREMIERS %%%%%
\usepackage{xlop} % JM pour les opérations
\opset{voperator=bottom}
%%% Table des nombres premiers  %%%%
\newcount\primeindex
\newcount\tryindex
\newif\ifprime
\newif\ifagain
\newcommand\getprime[1]{%
  \opcopy{2}{P0}%
  \opcopy{3}{P1}%
  \opcopy{5}{try}
  \primeindex=2
  \loop
  \ifnum\primeindex<#1\relax
  \testprimality
  \ifprime
  \opcopy{try}{P\the\primeindex}%
  \advance\primeindex by1
  \fi
  \opadd*{try}{2}{try}%
  \ifnum\primeindex<#1\relax
  \testprimality
  \ifprime
  \opcopy{try}{P\the\primeindex}%
  \advance\primeindex by1
  \fi
  \opadd*{try}{4}{try}%
  \fi
  \repeat
}
\newcommand\testprimality{%
  \begingroup
  \againtrue
  \global\primetrue
  \tryindex=0
  \loop
  \opidiv*{try}{P\the\tryindex}{q}{r}%
  \opcmp{r}{0}%
  \ifopeq \global\primefalse \againfalse \fi
  \opcmp{q}{P\the\tryindex}%
  \ifoplt \againfalse \fi
  \advance\tryindex by1
  \ifagain
  \repeat
  \endgroup
}

%%% Décomposition en nombres premiers %%%
\newcommand\primedecomp[2][nil]{%
  \begingroup
  \opset{#1}%
  \opcopy{#2}{NbtoDecompose}%
  \opabs{NbtoDecompose}{NbtoDecompose}%
  \opinteger{NbtoDecompose}{NbtoDecompose}%
  \opcmp{NbtoDecompose}{0}%
  \ifopeq
  Je refuse de décomposer zéro.
  \else
  \setbox1=\hbox{\opdisplay{operandstyle.1}%
    {NbtoDecompose}}%
  {\setbox2=\box2{}}%
  \count255=1
  \primeindex=0
  \loop
  \opcmp{NbtoDecompose}{1}\ifopneq
  \opidiv*{NbtoDecompose}{P\the\primeindex}{q}{r}%
  \opcmp{0}{r}\ifopeq
  \ifvoid2
  \setbox2=\hbox{%
    \opdisplay{intermediarystyle.\the\count255}%
    {P\the\primeindex}}%
  \else
  \setbox2=\vtop{%
    \hbox{\box2}
    \hbox{%
      \opdisplay{intermediarystyle.\the\count255}%
      {P\the\primeindex}}}
  \fi
  \opcopy{q}{NbtoDecompose}%
  \advance\count255 by1
  \setbox1=\vtop{%
    \hbox{\box1}
    \hbox{%
      \opdisplay{operandstyle.\the\count255}%
      {NbtoDecompose}}
  }%
  \else
  \advance\primeindex by1
  \fi
  \repeat
  \hbox{\box1
    \kern0.5\opcolumnwidth
    \opvline(0,0.75){\the\count255.25}
    \kern0.5\opcolumnwidth
    \box2}%
  \fi
  \endgroup
}

\usepackage{scratch3}
\usepackage{ProfCollege} % à appeler avant le paquet cancel sinon option clash
\usepackage{cancel}

\usepackage[tikz]{bclogo}

%%%%%%% PSTRICKS %%%%%%%
\usepackage{pstricks,pst-plot,pst-tree,pstricks-add}
\usepackage{pst-eucl}% permet de faire des dessins de géométrie simplement
\usepackage{pst-text}
\usepackage{pst-node,pst-all}
\usepackage{pst-func,pst-math,pst-bspline,pst-3dplot}  %%% POUR LE BAC %%%

\renewcommand{\pstEllipse}[5][]{% arc d'ellipse pour le sujet de Polynésie septembre 2013
  \psset{#1}
  \parametricplot{#4}{#5}{#2\space t cos mul #3\space t sin mul}
}

%%%%% TRACÉS DANS UN REPÈRE %%%%%
\newcommand{\vect}[1]{\overrightarrow{\,\mathstrut#1\,}}
\def\Oij{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath}\right)$}
\def\Oijk{$\left(\text{O}~;~\vect{\imath},~\vect{\jmath},~\vect{k}\right)$}
\def\Ouv{$\left(\text{O}~;~\vect{u},~\vect{v}\right)$}

\newcommand{\e}{\mathrm{\,e\,}} %%% POUR LE BAC %%% le e de l'exponentielle
\newcommand{\ds}{\displaystyle} %%% POUR LE BAC %%%

%%%%% COMMANDES SPRECIFIQUES %%%%%
\usepackage{esvect} %%% POUR LE BAC %%%
\newcommand{\vvt}[1]{\vv{\text{#1}}} %%% POUR LE BAC %%%
\newcommand{\vectt}[1]{\overrightarrow{\,\mathstrut\text{#1}\,}} %%% POUR LE BAC %%%

\usepackage{multirow} % fusionner plusieurs lignes de tableau
\usepackage{diagbox} % des diagonales dans une cellule de tableau
\usepackage{forest} % arbre en proba

%%%%% PROBABILITÉS %%%%%
% Structure servant à avoir l'événement et la probabilité.
\def\getEvene#1/#2\endget{$#1$}
\def\getProba#1/#2\endget{$#2$}

\usepackage{pifont} % symboles
\newcommand{\textding}[1]{\text{\ding{#1}}}

\usepackage{booktabs} % tableaux de qualité

\usepackage[french]{babel}


\Theme[CAN]{}{}{}{}
\begin{document}
\setcounter{nbEx}{1}
\pageDeGardeCan{nbEx}
\clearpage\begin{TableauCan}
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie sur $\mathbb{R}$ par : \\
        $f(x)=-6x+2$. &  $f^\prime(x)=\ldots$ &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie sur $\mathbb{R}$ par : \\         $f(x)=\dfrac{-2x}{15}-1$.\\          Déterminer $f'(x)$.   \\   &   &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie sur $\mathbb{R}$ par : \\
     $f(x)= -5x^2-x+8$. &  $f'(x)=\ldots$ &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie sur $\mathbb{R}$ par : \\            $f(x)=3x+x^3 $.\\     Déterminer $f'(x)$. &   &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie sur $[0;+\infty[$ par : \\            $f(x)=4\sqrt{x}$. \\            Déterminer $f'(x)$ pour $x\in ]0;+\infty[$ .\\      &   &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie sur $\mathbb{R}$ par : $f(x)= 2x+7+2x^2$. &  $f'(-3)=\ldots$ &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& La courbe représente une fonction $f$ et la droite est la tangente au point d'abscisse $0$.\\        Déterminer $f'(0)$. \\        \begin{tikzpicture}[baseline,scale = 0.5]    \tikzset{
      point/.style={
        thick,
        draw,
        cross out,
        inner sep=0pt,
        minimum width=5pt,
        minimum height=5pt,
      },
    }
    \clip (-10,-2) rectangle (4,12);	\draw[color ={black},line width = 2,>=latex,->] (-10,0)--(4,0);
	\draw[color ={black},line width = 2,>=latex,->] (0,-2)--(0,12);
	\draw[color ={black},opacity = 0.5] (-10,1)--(4,1);
	\draw[color ={black},opacity = 0.5] (-10,2)--(4,2);
	\draw[color ={black},opacity = 0.5] (-10,3)--(4,3);
	\draw[color ={black},opacity = 0.5] (-10,4)--(4,4);
	\draw[color ={black},opacity = 0.5] (-10,5)--(4,5);
	\draw[color ={black},opacity = 0.5] (-10,6)--(4,6);
	\draw[color ={black},opacity = 0.5] (-10,7)--(4,7);
	\draw[color ={black},opacity = 0.5] (-10,8)--(4,8);
	\draw[color ={black},opacity = 0.5] (-10,9)--(4,9);
	\draw[color ={black},opacity = 0.5] (-10,10)--(4,10);
	\draw[color ={black},opacity = 0.5] (-10,11)--(4,11);
	\draw[color ={black},opacity = 0.5] (-10,12)--(4,12);
	\draw[color ={black},opacity = 0.5] (-10,-1)--(4,-1);
	\draw[color ={black},opacity = 0.5] (-10,-2)--(4,-2);
	\draw[color ={black},opacity = 0.5] (1,-2)--(1,12);
	\draw[color ={black},opacity = 0.5] (2,-2)--(2,12);
	\draw[color ={black},opacity = 0.5] (3,-2)--(3,12);
	\draw[color ={black},opacity = 0.5] (4,-2)--(4,12);
	\draw[color ={black},opacity = 0.5] (-1,-2)--(-1,12);
	\draw[color ={black},opacity = 0.5] (-2,-2)--(-2,12);
	\draw[color ={black},opacity = 0.5] (-3,-2)--(-3,12);
	\draw[color ={black},opacity = 0.5] (-4,-2)--(-4,12);
	\draw[color ={black},opacity = 0.5] (-5,-2)--(-5,12);
	\draw[color ={black},opacity = 0.5] (-6,-2)--(-6,12);
	\draw[color ={black},opacity = 0.5] (-7,-2)--(-7,12);
	\draw[color ={black},opacity = 0.5] (-8,-2)--(-8,12);
	\draw[color ={black},opacity = 0.5] (-9,-2)--(-9,12);
	\draw[color ={black},opacity = 0.5] (-10,-2)--(-10,12);
	\draw[color ={gray},opacity = 0.3] (-10,0)--(4,0);
	\draw[color ={gray},opacity = 0.3] (-10,0)--(4,0);
	\draw[color ={gray},opacity = 0.3] (0,-2)--(0,12);
	\draw[color ={gray},opacity = 0.3] (0,-2)--(0,12);
	\draw[color ={gray},opacity = 0.3] (-6,-2)--(-6,12);
	\draw[color ={gray},opacity = 0.3] (-8,-2)--(-8,12);
	\draw[color ={gray},opacity = 0.3] (-10,-2)--(-10,12);
	\draw[color ={black},line width = 2] (2,-0.2)--(2,0.2);
	\draw[color ={black},line width = 2] (-2,-0.2)--(-2,0.2);
	\draw[color ={black},line width = 2] (-4,-0.2)--(-4,0.2);
	\draw[color ={black},line width = 2] (-6,-0.2)--(-6,0.2);
	\draw[color ={black},line width = 2] (-8,-0.2)--(-8,0.2);
	\draw[color ={black},line width = 2] (-0.2,2)--(0.2,2);
	\draw[color ={black},line width = 2] (-0.2,4)--(0.2,4);
	\draw[color ={black},line width = 2] (-0.2,6)--(0.2,6);
	\draw[color ={black},line width = 2] (-0.2,8)--(0.2,8);
	\draw[color ={black},line width = 2] (-0.2,10)--(0.2,10);
	\draw [color={black}] (2,-0.5) node[anchor = center,scale=0.8, rotate = 0] {1};
	\draw [color={black}] (-2,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-1};
	\draw [color={black}] (-4,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-2};
	\draw [color={black}] (-6,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-3};
	\draw [color={black}] (-8,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-4};
	\draw [color={black}] (-0.5,2) node[anchor = center,scale=0.8, rotate = 0] {1};
	\draw [color={black}] (-0.5,4) node[anchor = center,scale=0.8, rotate = 0] {2};
	\draw [color={black}] (-0.5,6) node[anchor = center,scale=0.8, rotate = 0] {3};
	\draw [color={black}] (-0.5,8) node[anchor = center,scale=0.8, rotate = 0] {4};
	\draw [color={black}] (-0.5,10) node[anchor = center,scale=0.8, rotate = 0] {5};
	\draw [color={black}] (-0.3,-0.3) node[anchor = center,scale=1, rotate = 0] {O};	\draw[color={blue},line width = 2] (-2,10.59)--(-1.6,7.59)--(-1.2,5.44)--(-0.8,3.9)--(-0.4,2.79)--(0,2)--(0.4,1.43)--(0.8,1.03)--(1.2,0.74)--(1.6,0.53)--(2,0.38)--(2.4,0.27)--(2.8,0.19)--(3.2,0.14)--(3.6,0.1)--(4,0.07);	\draw[color={red},line width = 2] (-7.2,14)--(-6.8,13.33)--(-6.4,12.67)--(-6,12)--(-5.6,11.33)--(-5.2,10.67)--(-4.8,10)--(-4.4,9.33)--(-4,8.67)--(-3.6,8)--(-3.2,7.33)--(-2.8,6.67)--(-2.4,6)--(-2,5.33)--(-1.6,4.67)--(-1.2,4)--(-0.8,3.33)--(-0.4,2.67)--(0,2)--(0.4,1.33)--(0.8,0.67)--(1.2,0)--(1.6,-0.67)--(2,-1.33)--(2.4,-2)--(2.8,-2.67)--(3.2,-3.33);\end{tikzpicture}\\ &  $f'(0)=\ldots$ &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie  par : \\                   $f(x)=\dfrac{1}{-8x^2+2}$.\\                    Déterminer $f'(x)$.\\      &   &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& Soit $f$ la fonction définie  par : \\            $f(x)=-6x^2-1+\dfrac{5}{x}$. \\            Déterminer  $f'(x)$ (écrire le résultat sous la fomre d'un seul quotient).\\      &   &\tabularnewline \hline
\thenbEx  \addtocounter{nbEx}{1}& On donne la représentation graphique d'une fonction $f$. \\
    Dresser le tableau de signes de sa fonction dérivée $f^\prime$.\\\begin{tikzpicture}[baseline,scale = 0.55]    \tikzset{
      point/.style={
        thick,
        draw,
        cross out,
        inner sep=0pt,
        minimum width=5pt,
        minimum height=5pt,
      },
    }
    \clip (-4,-3) rectangle (7,5);	\draw[color ={black},line width = 2,>=latex,->] (-4,0)--(7,0);
	\draw[color ={black},line width = 2,>=latex,->] (0,-3)--(0,5);
	\draw[color ={gray},opacity = 0.3] (-4,0)--(7,0);
	\draw[color ={gray},opacity = 0.3] (-4,1)--(7,1);
	\draw[color ={gray},opacity = 0.3] (-4,2)--(7,2);
	\draw[color ={gray},opacity = 0.3] (-4,3)--(7,3);
	\draw[color ={gray},opacity = 0.3] (-4,4)--(7,4);
	\draw[color ={gray},opacity = 0.3] (-4,5)--(7,5);
	\draw[color ={gray},opacity = 0.3] (-4,0)--(7,0);
	\draw[color ={gray},opacity = 0.3] (-4,-1)--(7,-1);
	\draw[color ={gray},opacity = 0.3] (-4,-2)--(7,-2);
	\draw[color ={gray},opacity = 0.3] (-4,-3)--(7,-3);
	\draw[color ={gray},opacity = 0.3] (0,-3)--(0,5);
	\draw[color ={gray},opacity = 0.3] (1,-3)--(1,5);
	\draw[color ={gray},opacity = 0.3] (2,-3)--(2,5);
	\draw[color ={gray},opacity = 0.3] (3,-3)--(3,5);
	\draw[color ={gray},opacity = 0.3] (4,-3)--(4,5);
	\draw[color ={gray},opacity = 0.3] (5,-3)--(5,5);
	\draw[color ={gray},opacity = 0.3] (6,-3)--(6,5);
	\draw[color ={gray},opacity = 0.3] (7,-3)--(7,5);
	\draw[color ={gray},opacity = 0.3] (0,-3)--(0,5);
	\draw[color ={gray},opacity = 0.3] (-1,-3)--(-1,5);
	\draw[color ={gray},opacity = 0.3] (-2,-3)--(-2,5);
	\draw[color ={gray},opacity = 0.3] (-3,-3)--(-3,5);
	\draw[color ={gray},opacity = 0.3] (-4,-3)--(-4,5);
	\draw[color ={black},line width = 2] (1,-0.2)--(1,0.2);
	\draw[color ={black},line width = 2] (2,-0.2)--(2,0.2);
	\draw[color ={black},line width = 2] (3,-0.2)--(3,0.2);
	\draw[color ={black},line width = 2] (4,-0.2)--(4,0.2);
	\draw[color ={black},line width = 2] (5,-0.2)--(5,0.2);
	\draw[color ={black},line width = 2] (6,-0.2)--(6,0.2);
	\draw[color ={black},line width = 2] (-1,-0.2)--(-1,0.2);
	\draw[color ={black},line width = 2] (-2,-0.2)--(-2,0.2);
	\draw[color ={black},line width = 2] (-3,-0.2)--(-3,0.2);
	\draw[color ={black},line width = 2] (-0.2,1)--(0.2,1);
	\draw[color ={black},line width = 2] (-0.2,2)--(0.2,2);
	\draw[color ={black},line width = 2] (-0.2,3)--(0.2,3);
	\draw[color ={black},line width = 2] (-0.2,4)--(0.2,4);
	\draw[color ={black},line width = 2] (-0.2,-1)--(0.2,-1);
	\draw[color ={black},line width = 2] (-0.2,-2)--(0.2,-2);
	\draw [color={black}] (1,-0.5) node[anchor = center,scale=0.8, rotate = 0] {1};
	\draw [color={black}] (2,-0.5) node[anchor = center,scale=0.8, rotate = 0] {2};
	\draw [color={black}] (3,-0.5) node[anchor = center,scale=0.8, rotate = 0] {3};
	\draw [color={black}] (4,-0.5) node[anchor = center,scale=0.8, rotate = 0] {4};
	\draw [color={black}] (5,-0.5) node[anchor = center,scale=0.8, rotate = 0] {5};
	\draw [color={black}] (6,-0.5) node[anchor = center,scale=0.8, rotate = 0] {6};
	\draw [color={black}] (-1,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-1};
	\draw [color={black}] (-2,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-2};
	\draw [color={black}] (-3,-0.5) node[anchor = center,scale=0.8, rotate = 0] {-3};
	\draw [color={black}] (-0.5,1) node[anchor = center,scale=0.8, rotate = 0] {1};
	\draw [color={black}] (-0.5,2) node[anchor = center,scale=0.8, rotate = 0] {2};
	\draw [color={black}] (-0.5,3) node[anchor = center,scale=0.8, rotate = 0] {3};
	\draw [color={black}] (-0.5,4) node[anchor = center,scale=0.8, rotate = 0] {4};
	\draw [color={black}] (-0.5,-1) node[anchor = center,scale=0.8, rotate = 0] {-1};
	\draw [color={black}] (-0.5,-2) node[anchor = center,scale=0.8, rotate = 0] {-2};	\draw[color={blue},line width = 1.5] (-3,1)--(-2.9,0.94)--(-2.8,0.86)--(-2.7,0.77)--(-2.6,0.67)--(-2.5,0.56)--(-2.4,0.45)--(-2.3,0.33)--(-2.2,0.22)--(-2.1,0.1)--(-2,0);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-3.125,1.125)--(-2.875,0.875);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-3.125,0.875)--(-2.875,1.125);	\draw[color={blue},line width = 1.5] (-2,0)--(-1.9,-0.14)--(-1.8,-0.34)--(-1.7,-0.58)--(-1.6,-0.85)--(-1.5,-1.13)--(-1.4,-1.39)--(-1.3,-1.63)--(-1.2,-1.82)--(-1.1,-1.95)--(-1,-2);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-2.125,0.125)--(-1.875,-0.125);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-2.125,-0.125)--(-1.875,0.125);	\draw[color={blue},line width = 1.5] (-1,-2)--(-0.9,-1.95)--(-0.8,-1.82)--(-0.7,-1.63)--(-0.6,-1.39)--(-0.5,-1.13)--(-0.4,-0.85)--(-0.3,-0.58)--(-0.2,-0.34)--(-0.1,-0.14)--(0,0);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-1.125,-1.875)--(-0.875,-2.125);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-1.125,-2.125)--(-0.875,-1.875);	\draw[color={blue},line width = 1.5] (0,0)--(0.1,0.09)--(0.2,0.17)--(0.3,0.24)--(0.4,0.3)--(0.5,0.34)--(0.6,0.38)--(0.7,0.42)--(0.8,0.45)--(0.9,0.47)--(1,0.5)--(1.1,0.53)--(1.2,0.55)--(1.3,0.58)--(1.4,0.62)--(1.5,0.66)--(1.6,0.7)--(1.7,0.76)--(1.8,0.83)--(1.9,0.91)--(2,1);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-0.125,0.125)--(0.125,-0.125);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (-0.125,-0.125)--(0.125,0.125);	\draw[color={blue},line width = 1.5] (2,1)--(2.1,1.1)--(2.2,1.2)--(2.3,1.3)--(2.4,1.4)--(2.5,1.5)--(2.6,1.6)--(2.7,1.7)--(2.8,1.8)--(2.9,1.9)--(3,2);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (1.875,1.125)--(2.125,0.875);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (1.875,0.875)--(2.125,1.125);	\draw[color={blue},line width = 1.5] (3,2)--(3.1,2.14)--(3.2,2.34)--(3.3,2.58)--(3.4,2.85)--(3.5,3.13)--(3.6,3.39)--(3.7,3.63)--(3.8,3.82)--(3.9,3.95)--(4,4);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (2.875,2.125)--(3.125,1.875);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (2.875,1.875)--(3.125,2.125);	\draw[color={blue},line width = 1.5] (4,4)--(4.1,3.9)--(4.2,3.62)--(4.3,3.2)--(4.4,2.69)--(4.5,2.13)--(4.6,1.55)--(4.7,1.01)--(4.8,0.54)--(4.9,0.19)--(5,0);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (3.875,4.125)--(4.125,3.875);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (3.875,3.875)--(4.125,4.125);	\draw[color={blue},line width = 1.5] (5,0)--(5.1,-0.1)--(5.2,-0.22)--(5.3,-0.33)--(5.4,-0.45)--(5.5,-0.56)--(5.6,-0.67)--(5.7,-0.77)--(5.8,-0.86)--(5.9,-0.94)--(6,-1);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (4.875,0.125)--(5.125,-0.125);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (4.875,-0.125)--(5.125,0.125);	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (5.875,-0.875)--(6.125,-1.125);
	\draw[color ={{blue}},line width = 1.25,opacity = 0.8] (5.875,-1.125)--(6.125,-0.875);
	\draw [color={black}] (-0.3,-0.3) node[anchor = center,scale=1, rotate = 0] {O};\end{tikzpicture} &   &\tabularnewline \hline
\end{TableauCan}
\addtocounter{nbEx}{-1}

\clearpage

\begin{Correction}
\begin{enumerate}
\item On reconnaît une fonction affine de la forme $f(x)=mx+p$ avec $m=-6$ et $p=2$.\\
        La fonction dérivée est donnée par $f'(x)=m$, soit ici $f'(x)=-6$.
\item On reconnaît une fonction affine de la forme $f(x)=mx+p$ avec $m=\dfrac{-2}{15}$  et $p=-1$.\\
            La fonction dérivée est donnée par $f'(x)=m$, soit ici $f'(x)=\dfrac{-2}{15}$.
\item $f$ est une fonction polynôme du second degré de la forme $f(x)=ax^2+bx+c$.\\
    La fonction dérivée est donnée par la somme des dérivées des fonctions $u$ et $v$ définies par $u(x)=-5x^2$ et $v(x)=-x+8$.\\
     Comme $u'(x)=-10x$ et $v'(x)=-1$, on obtient  $f'(x)=-10x-1$.
\item $f$ est une fonction polynôme du troisième degré de la forme $f(x)=ax^3+bx^2+cx+d$ avec $b=0$ et $d=0$.\\
        La fonction dérivée est donnée par la somme des dérivées des fonctions $u$ et $v$  définies par $u(x)=x^3$ et $v(x)=3x$.\\
         Comme $u'(x)=3x^2$, $v'(x)=3$, on obtient  $f'(x)=3x^2+3$.
\item $f(x)=4\sqrt{x}=4\times\sqrt{x}$.\\
          Or  $x\longmapsto \sqrt{x}$ a pour dérivée $x\longmapsto \dfrac{1}{2\sqrt{x}}$.\\
          Par conséquent, $f'(x)=4\times \dfrac{1}{2\sqrt{x}}=\dfrac{4}{2\sqrt{x}}=\dfrac{2}{\sqrt{x}}$.
\item $f$ est une fonction polynôme du second degré de la forme $f(x)=ax^2+bx+c$.\\
    La fonction dérivée est donnée par la somme des dérivées des fonctions $u$ et $v$ définies par $u(x)=2x^2$ et $v(x)=2x+7$.\\
     Comme $u'(x)=4x$ et $v'(x)=2$, on obtient  $f'(x)=4x+2$. \\
     Ainsi, $f'(-3)=4\times (-3)+2=-10$.
\item $f'(0)$ est donné par le coefficient directeur de la tangente à la courbe au point d'abscisse $0$, soit $\dfrac{-5}{3}=-\dfrac{5}{3}$.
\item $f$est de la forme $\dfrac{1}{u}$ avec $u(x)=-8x^2+2$.\\
                         Or  $\left(\dfrac{1}{u}\right)'=\dfrac{-u'}{u^2}$.\\
                  On a  $u(x)=-8x^2+2$ et $u'(x)=-16x$. On en déduit,
                  $f'(x)= \dfrac{-(-16)x}{(-8x^2+2)^2}$$=\dfrac{16x}{(-8x^2+2)^2}$.
\item $f$est de la forme $u+v$ avec $u(x)=-6x^2-1$ et $v(x)=\dfrac{5}{x}$.\\
                 On a $u'(x)=-12x$ et $v'(x)=\dfrac{-5}{x^2}$.\\
          Ainsi,
          $f'(x)= -12x+\dfrac{-5}{x^2}=\dfrac{-12x^3}{x^2}+\dfrac{-5}{x^2}=\dfrac{-12x^3-5}{x^2}$.
\item L'ensemble de définition de $f$ est $[-3\,;\,6]$.\\
    Lorsque la fonction $f$ est croissante, $f^\prime$ est positive et lorsque $f$ est décroissante, $f^\prime$ est négative.\\\begin{tikzpicture}[baseline, scale=0.75]
    \tkzTabInit[lgt=8,deltacl=0.8,espcl=3.5]{ $x$ / 2, $f^\prime(x)$ / 2}{ $-3$, $-1$, $4$, $6$}
	\tkzTabLine{  , -, z, +, z, -}
	\end{tikzpicture}

\end{enumerate}

\clearpage
\end{Correction}
\end{document}

% Local Variables:
% TeX-engine: luatex
% End: