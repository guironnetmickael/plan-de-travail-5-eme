\input mestrucscours.tex
\usepackage{srcltx}
\setmarginsrb{1.7cm}{2.1cm}{1.7cm}{2cm}{0cm}{0cm}{0cm}{-0.5cm}

\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\renewcommand{\arraystretch}{1.5}
\setcounter{chapter}{-1}
%\includeonly{chapitre10/fct_troisieme_degre}

\begin{document}

\chapter{Pourcentages}
\label{chap0}



\section{Proportions}

On considère une population E d'individus.

On appelle effectif de la population E, le nombre d'individus de E, on le note $n_\text{E}$.

\medskip

Une sous-population A de E est une partie de la population de
E : on a donc $n_\text{A}\leq n_\text{E}$.

\postit{Définition}{
La proportion (ou fréquence) d'une sous-population A dans la population E (ou proportion des individus de A parmi ceux de E) est le nombre noté $p$ donné par :
\begin{center}
$p=\dfrac{n_\text{A}}{n_\text{E}}$
\end{center}
 où
E est dite la population de référence.
}

\medskip

{\bf Remarques :}

\begin{itemize}
  \item [$\bullet$] Une proportion est (en général)  un nombre compris entre $0$ et $1$.

  \medskip

  \item [$\bullet$] Une proportion est souvent exprimée en pourcentage. Le
    pourcentage est la fraction de dénominateur $100$ et n'est
    qu'une des écritures possibles d'un nombre décimal.
\end{itemize}

\medskip


\section{Proportion d'une proportion}

\postit{Propriété}{
On considère 3 populations A, E et F où A est une
sous-population de E et E une sous-population de F.

Si $p$ est la proportion de E dans F et $p'$ celle de A dans E, alors la proportion $P$ de A dans F est :
\begin{center}
$P=p\times p'$
\end{center}
}

{\bf Représentation :}

\begin{center}
\begin{tikzpicture}[line cap=round,line join=round,>=latex',x=1.0cm,y=1cm]
\clip(-3.06,-2.6199999999999974) rectangle (11.68,2.8999999999999995);
\draw [rotate around={27.52522574374434:(-0.9499999999999998,0.7300000000000001)},line width=1.2pt,color=red] (-0.9499999999999998,0.7300000000000001) ellipse (1.058067338641908cm and 0.6917416375358435cm);
\draw [rotate around={20.90407908333266:(-1.0,0.7300000000000004)},line width=1.2pt,color=blue] (-1.0,0.7300000000000004) ellipse (1.868439911559739cm and 1.0559203109654458cm);
\draw [rotate around={21.10008374846645:(-0.26000000000000006,0.6699999999999999)},line width=1.2pt,color=green] (-0.26000000000000006,0.6699999999999999) ellipse (2.6957053431482314cm and 1.8376961928126003cm);
\draw [color=red](-0.66,1.4) node[anchor=north west] {A};
\draw [color=blue](0.12,1.7) node[anchor=north west] {E};
\draw [color=green](1.42,2.2) node[anchor=north west] {F};
\draw [rotate around={27.525225743744333:(6.109999999999999,0.6499999999999994)},line width=1.2pt,color=red] (6.109999999999999,0.6499999999999994) ellipse (1.058067338641912cm and 0.6917416375358458cm);
\draw [rotate around={20.90407908333265:(6.060000000000001,0.6500000000000006)},line width=1.2pt,color=blue] (6.060000000000001,0.6500000000000006) ellipse (1.8684399115597436cm and 1.055920310965448cm);
\draw [color=red](6.4,1.4) node[anchor=north west] {A};
\draw [color=blue](7.18,1.7) node[anchor=north west] {E};
\draw [->,line width=1.2pt] (2.2332378602427103,0.61) -- (4.377998810464226,0.61);
\draw [rotate around={27.525225743744336:(10.449999999999994,0.5299999999999984)},line width=1.2pt,color=red] (10.449999999999994,0.5299999999999984) ellipse (1.0580673386418729cm and 0.6917416375358205cm);
\draw [color=red](10.74,1.3) node[anchor=north west] {A};
\draw [line width=1.2pt,->] (7.639176011946104,0.53) -- (9.516942179382466,0.53);
\draw (-0.5837788373524802,-1.2826809947591669)-- (-0.6,-1.8);
\draw (-0.6,-1.8)-- (10.26,-1.8);
\draw [->] (10.26,-1.8) -- (10.272598363922747,-0.24386662084082666);
\draw (2.9,1.3800000000000003) node[anchor=north west] {$\times p$};
\draw (8.36,1.3200000000000005) node[anchor=north west] {$\times p'$};
\draw (4.22,-1.759999999999998) node[anchor=north west] {$\times (p\times p')$};
\end{tikzpicture}
\end{center}








\section{Taux d'évolution et coefficient multiplicateur}

On considère deux valeurs numériques réelles strictement positives $\text{V}_\text{I}$ et $\text{V}_\text{F}$.

La valeur $\text{V}_\text{I}$ est la valeur initiale et $\text{V}_\text{F}$ la valeur finale.


\subsection{Evolution et pourcentages}

\postit{Définition}{
On appelle {\bf taux d'évolution} de $\text{V}_\text{I}$ à $\text{V}_\text{F}$, le nombre T
défini par :
\begin{center}
$\text{T}=\dfrac{\text{V}_\text{F}-\text{V}_\text{I}}{\text{V}_\text{I}}$
\end{center}
}


{\bf Remarques :}

\begin{itemize}
    \item [$\bullet$] Un taux d'évolution n'a pas d'unité et peut être donné
sous forme de fraction, sous forme décimale ou sous forme de
pourcentage;
    \item [$\bullet$] Un taux d'évolution positif est un taux d'augmentation
et un taux d'évolution négatif est un taux de diminution ou de baisse.
    \item [$\bullet$] Un taux d'évolution s'exprime toujours par rapport à
la valeur initiale.
\end{itemize}



\subsection{Coefficient multiplicateur}

\postit{Définition}{
On appelle {\bf coefficient multiplicateur} de $\text{V}_\text{I}$ à $\text{V}_\text{F}$ le nombre $\text{CM}$ tel que : $\text{V}_\text{I}\times \text{CM}=\text{V}_\text{F}$.\\
Ainsi : $\text{CM}=\dfrac{\text{V}_\text{F}}{\text{V}_\text{I}}$
}

\medskip

\postit{Propriété}{
Soit T le taux d'évolution entre $\text{V}_\text{I}$ et $\text{V}_\text{F}$. Ainsi : $\text{CM}=1+T$
}

\begin{center}
\begin{tikzpicture}
\node[rectangle, draw, rounded corners=4pt] (VI) at (-4,0){
\renewcommand{\arraystretch}{1}
\middlexcolumn
\begin{tabular}{c}
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
   Valeur initiale \\
 $\text{V}_\text{I}$
\end{tabular}
};
\node[rectangle, draw, rounded corners=4pt] (VF) at (4,0){\renewcommand{\arraystretch}{1}
\middlexcolumn
\begin{tabular}{c}
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
   Valeur finale \\
 $\text{V}_\text{F}$
\end{tabular}};
\node[rectangle,draw] (def) at (0,-0.5){$\text{CM}=1+\text{T}$};
\node [ellipse,draw](CM) at (0,1){{\large $\times\text{CM}$}};
\draw[->,>=stealth',thick,rounded corners=4pt] (VI.north)|-(CM)-|(VF.north);
\end{tikzpicture}
\end{center}

{\bf Remarques :}

\begin{itemize}
  \item [$\bullet$] Le coefficient multiplicateur est strictement positif.
  \item [$\bullet$] Un coefficient multiplicateur supérieur à 1 correspond à une hausse.
  \item [$\bullet$] Un coefficient multiplicateur inférieur à 1 correspond à une baisse.
\end{itemize}


\postit{Propriété}{
Soit CM le coefficient multiplicateur tel que $\text{V}_\text{I}\times\text{CM}=\text{V}_\text{F}$.

Le taux d'évolution entre les valeurs $\text{V}_\text{I}$ et $\text{V}_\text{F}$ est :
$\text{T}=\dfrac{\text{V}_\text{F}-\text{V}_\text{I}}{\text{V}_\text{I}}$
}

\subsection{Indice}

\postit{Définition}{
Soit $\text{V}_1$ et $\text{V}_2$ la valeur initiale et la valeur finale d'une grandeur.

On définit \textbf{l'indice base 100 de cette grandeur} en associant :

\medskip

\begin{minipage}{7cm}
\begin{itemize}
  \item [$\bullet$] à $\text{V}_1$ l'indice $\text{I}_1=100$;
  \item [$\bullet$] à $\text{V}_2$ l'indice $\text{I}_2$ tel que $\dfrac{\text{I}_2}{\text{I}_1}=\dfrac{\text{V}_2}{\text{V}_1}=\text{CM}$.
\end{itemize}

\end{minipage}
\hspace{1cm}
\begin{minipage}{7cm}
\begin{center}
  \renewcommand{\arraystretch}{2}
  \middlexcolumn
  \begin{tabularx}{6cm}{|X|X|X|}
    \hline
    \textbf{Valeurs} &  $\text{V}_1$&$\text{V}_2$\\
    \hline
     \textbf{Indices} & $\text{I}_1=100$&$\text{I}_2$ \\
    \hline
  \end{tabularx}
\end{center}
\end{minipage}

}

\medskip

\textbf{Remarques :}
\begin{itemize}
  \item [$\bullet$] Le taux d'évolution entre les indices $\text{I}_1$ et $\text{I}_2$ est le même que celui entre les valeurs $\text{V}_1$ et $\text{V}_2$;
  \item [$\bullet$] Le tableau précédent est un tableau de proportionnalité.
\end{itemize}



\section{Evolutions successives et évolution réciproque}
\subsection{Succession de deux évolutions}
\postit{Définition}{
Soit $\text{T}_1$ le taux d'évolution entre deux valeurs $\text{V}_0$ à $\text{V}_1$ et $\text{T}_2$ le taux d'évolution entre les  valeurs $\text{V}_1$ à $\text{V}_2$

L'évolution globale de $\text{V}_0$ à $\text{V}_2$, noté $\text{T}_\text{G}$, a pour coefficient multiplicateur $\text{CM}_\text{G}$ avec :
\begin{center}
$\text{CM}_\text{G}=\text{CM}_\text{1}\times \text{CM}_\text{2}$
\end{center}
}

\begin{center}
\begin{tikzpicture}
\node[rectangle, draw, rounded corners=4pt] (V0) at (-6,0){$\text{V}_0$};
\node[rectangle, draw, rounded corners=4pt] (V1) at (0,0){$\text{V}_1$};
\node[rectangle, draw, rounded corners=4pt] (V2) at (6,0){$\text{V}_2$};
\node [ellipse,draw](CM1) at (-2,-1){{\large $\times\text{CM}_1$}};
\node [ellipse,draw](CM2) at (2,-1){{\large $\times\text{CM}_2$}};
\node [ellipse,draw](CMG) at (0,1){{\large $\times\text{CM}_\text{G}$}};
\draw[>=stealth',thick,rounded corners=4pt] (V0)|-(CM1);
\draw[->,>=stealth',thick,rounded corners=4pt] (CM1)-|(V1);
\draw[>=stealth',thick,rounded corners=4pt] (V1)|-(CM2);
\draw[->,>=stealth',thick,rounded corners=4pt] (CM2)-|(V2);
\draw[->,>=stealth',thick,rounded corners=4pt] (V0.north)|- (CMG) -|(V2.north);
%\draw[->,>=stealth',thick,rounded corners=4pt] (CM)-|(VF);
%\draw[->,>=stealth',thick] (VI) to[bend left] (VF);
\end{tikzpicture}
\end{center}




\subsection{Evolution réciproque}
\postit{Définition}{
Soit T le taux d'évolution entre deux valeurs $\text{V}_\text{I}$ à $\text{V}_\text{F}$ et CM son coefficient multiplicateur associé.

L'évolution T' de $\text{V}_\text{F}$ à $\text{V}_\text{I}$ est appelé le taux d'évolution réciproque de T dont le coefficient multiplicateur CM' associé est : \[\text{CM'}=\dfrac{1}{\text{CM'}}\]
}

\begin{center}
\begin{tikzpicture}
\node[rectangle, draw, rounded corners=4pt] (VI) at (-4,0){
\renewcommand{\arraystretch}{1}
\middlexcolumn
\begin{tabular}{c}
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
   Valeur initiale \\
 $\text{V}_\text{I}$
\end{tabular}
};
\node[rectangle, draw, rounded corners=4pt] (VF) at (4,0){\renewcommand{\arraystretch}{1}
\middlexcolumn
\begin{tabular}{c}
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
   Valeur finale \\
 $\text{V}_\text{F}$
\end{tabular}};
%\node[rectangle,draw] (def) at (0,-0.5){$\text{CM}=1+\text{T}$};
\node [ellipse,draw](CM) at (0,1){{\large $\times\text{CM}$}};
\node [ellipse,draw](CR) at (0,-1){{\large $\times\text{CM'}$}};
\draw[->,>=stealth',thick,rounded corners=4pt] (VI.north)|-(CM)-|(VF.north);
\draw[->,>=stealth',thick,rounded corners=4pt] (VF)|-(CR)-|(VI);
\end{tikzpicture}
\end{center}


\textbf{Remarques :}

\smallskip

\begin{itemize}
  \item [$\bullet$] Deux évolutions sont réciproques lorsque le coefficient multiplicateur global de ces évolutions vaut $1$;

      \smallskip

    \item [$\bullet$] Le taux d'évolution réciproque de $\text{V}_\text{F}$ à $\text{V}_\text{I}$ vérifie l'égalité : $1+\text{T}'=\dfrac{1}{1+\text{T}}$;

    \smallskip

    \item [$\bullet$] Une hausse de T\,\% n'est pas compensée par une baisse de T\,\%.
\end{itemize}



\end{document} 