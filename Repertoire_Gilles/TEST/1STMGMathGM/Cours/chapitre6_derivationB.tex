\input mestrucscours.tex

\setmarginsrb{1.7cm}{2.1cm}{1.7cm}{2cm}{0cm}{0cm}{0cm}{-0.5cm}

\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\renewcommand{\arraystretch}{1.5}
\setcounter{chapter}{5}
%\includeonly{chapitre10/fct_troisieme_degre}

\begin{document}
\chapter{Dérivation}
\label{chap11}

\subsection{Taux de variation}
\postit{Définition}{
Soit $f$ une fonction définie sur un intervalle I et $a$ un réel appartenant à I.

Soit $h$ un réel non nul tel que $a+h\in \text{I}$.

\medskip

Le taux de variation de $f$ entre $a$ et $a+h$ est le rapport (quotient) $t(h)$ défini par :
\begin{center}
$t(h)=\dfrac{f(a+h)-f(a)}{h}$
\end{center}
}

\subsection{Interprétation graphique}

\begin{minipage}{8.5cm}
\postit{Définition}{
On considère deux points A et M d'une courbe $\mathscr{C}_f$ d'abscisses respectives $a$ et $a+h$.

La droite $(\text{AM})$ est appelée sécante à la courbe $\mathscr{C}_f$.
}

\medskip

Le taux de variation de $f$ entre $a$ et $a+h$ est le coefficient directeur de la sécante $(\text{AM})$ :
\begin{center}
$\dfrac{\text{DV}}{\text{DH}}=\dfrac{y_\text{M}-y_\text{A}}{x_\text{M}-x_\text{A}}=\dfrac{f(a+h)-f(a)}{h}$
\end{center}
\end{minipage}
\hfill
\begin{minipage}{8.3cm}
\begin{center}
\psset{xunit=0.55cm,yunit=0.08cm,algebraic=true,dimen=middle,dotstyle=o,dotsize=3pt 0,linewidth=0.8pt,arrowsize=3pt 2,arrowinset=0.25}
\begin{pspicture*}(-2.5,-20)(12.5,60)
\psaxes[linewidth=1.2pt,labelFontSize=\scriptstyle,xAxis=true,yAxis=true,labels=none,Dx=1,Dy=10,ticksize=0pt 0,subticks=2]{->}(0,0)(-1,-10)(10,55)
\rput[tl](-0.8,-2.7){O}
\rput[tl](1.72,22){A}
\rput[tl](4.35,36){M}
\psline[linewidth=1pt,linestyle=dashed](4.8,29.71)(0,29.71)
\psline[linewidth=1pt,linestyle=dashed](4.8,29.71)(4.8,0)
\psline[linewidth=1pt,linestyle=dashed](2,15.28)(0,15.28)
\psline[linewidth=1pt,linestyle=dashed](2,15.28)(2,0)
\rput[tl](1.89,-2){$a$}
\rput[tl](-1.3,18.5){$f(a)$}
\rput[tl](4.35,-2){$a+h$}
\rput[tl](-2.5,33.7){$f(a+h)$}
\psplot[linewidth=1.5pt,linecolor=blue]{-2}{8}{(--14.11--14.43*x)/2.81}
\psplot[linewidth=1.5pt,linecolor=red,plotpoints=200]{-0.2}{5.5}{(x-0.38)^(3)-4*(x-0.38)^(2)+21.49}
\rput[tl](4.5,53.37){\large \red $\mathscr{C}_f$}
\psline[linewidth=1.2pt]{->}(2.5,-10)(4.8,-10)
\psline{->}(4.8,-10)(2,-10)
\rput[tl](2.7,-11){$\text{DH}=h$}
\psline[linewidth=1.2pt]{->}(6.29,29.88)(6.31,15.08)
\psline{->}(6.32,15.08)(6.3,29.88)
\psline[linewidth=1pt,linestyle=dashed](2,15.28)(6.32,15.08)
\psline[linewidth=1pt,linestyle=dashed](4.8,29.71)(6.29,29.88)
\rput[tl](6.49,26.75){\small $\text{DV}=f(a+h)-f(a)$}
\begin{scriptsize}
\psdots[dotstyle=*,dotscale=2](2,15.28)(4.8,29.71)
\end{scriptsize}
\end{pspicture*}
\end{center}
\end{minipage}

\medskip

\section{Nombre dérivé et tangente à une courbe}

\subsection{Nombre dérivé}


\begin{minipage}{10cm}
\postit{Définition}{
On dit que $f$ est {\bf dérivable} en $a$ lorsque le taux de variation $t(h)$ admet comme limite un nombre réel quand $h$ tend vers $0$.

Ce nombre, noté $f'(a)$ est appelé {\bf nombre dérivé de ${\boldsymbol{f}}$} en $a$.

On a ainsi :
\[\lim_{h\to0}{\dfrac{f(a+h)-f(a)}{h}}=f'(a)\]
}\end{minipage}
\hspace{0.1cm}
\begin{minipage}{6.5cm}
\postit{Commentaire}{$\lim_{h\to0}{t(h)}{=\ell}$ se lit \og limite quand $h$ tend vers 0 de $t(h)$ égale $\ell$.

Cela signifie que lorsque le nombre $h$ devient très proche de $0$, le nombre $t(h)$ prend des valeurs très voisines de $\ell$ (aussi proche que l'on veut).}
\end{minipage}


\subsection{Tangente à une courbe}


\begin{minipage}{10cm}
\postit{Définition}{
Soit $f$ une fonction dérivable en $a$, $\mathscr{C}_f$ sa courbe représentative et A le point de $\mathscr{C}_f$ d'abscisse $a$.

\medskip

{\bf La tangente à la courbe ${\boldsymbol{\mathscr{C}_f}}$ au point A} est la droite passant par $A$  de coefficient directeur $f'(a)$. Son équation réduite est de la forme : $$y=f'(a)(x-a)+f(a)$$\vspace*{-0.5cm}
}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}{6cm}
\postit{Commentaire}{
Aux alentours du point A, la tangente est la droite la plus proche de la courbe $\mathscr{C}_f$.}
\end{minipage}

\bigskip


\section{Fonction dérivée}

\subsection{Lien entre le sens de variation d'une fonction et le signe de sa dérivée}


\postit{Définition}{Soit $f$ une fonction dérivable sur un un intervalle I.

La fonction qui, à tout réel $x$ de I, associe le nombre dérivé $f'(x)$ est appelée {\bf fonction dérivée } de $f$.

On la note $f'$.
}

\medskip

\postit{Propriétés}{
Soit $f$ une fonction dérivable sur un intervalle $I$.

\medskip

\begin{itemize}
  \item [$\bullet$] Si pour tout $x$ de $I$, on a $f'(x)\geq0$, alors $f$ est croissante sur $I$ ;
  \item [$\bullet$] Si pour tout $x$ de $I$, on a $f'(x)\leq0$, alors $f$ est décroissante sur $I$ ;
  \item [$\bullet$] Si pour tout $x$ de $I$, on a $f'(x)=0$, alors $f$ est constante sur $I$.
\end{itemize}
}


\section{Calcul d'une fonction dérivée}

\begin{minipage}{8.5cm}

\renewcommand{\arraystretch}{2}
\middlexcolumn
\begin{center}
\begin{tabularx}{8cm}{|X|X|}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  \textbf{Fonction} $\boldsymbol{f}$ & \textbf{Dérivée} $\boldsymbol{f'}$  \\
  \hline
  $k$~ (constante) & 0 \\
  \hline
  $x$ & 1 \\
  \hline
  $x^2$ & $2x$\\
  \hline
    $x^3$ & $3x^2$\\
  \hline
\end{tabularx}
\end{center}
\end{minipage}
\begin{minipage}{8.5cm}





$f$ et $g$ sont deux fonctions dérivables sur un intervalle I.

\begin{center}
\begin{tabularx}{8cm}{|X|X|}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  \textbf{Fonction} & \textbf{Dérivée} \\
  \hline
  $f+g$ & $f'+g'$ \\
  \hline
  $kf$ ($k$ constante)& $kf'$ \\
  \hline
\end{tabularx}
\end{center}


\end{minipage}


\section{Applications aux fonctions polynômes de degré 2 et 3}


\postit{Propriétés}{
\medskip

\begin{itemize}
  \item [$\bullet$] Les fonctions polynômes de degré 2 définies sur $\R$ par : $f(x)=ax^2+bx+c$ sont dérivables sur $\R$ et pour tout réel $x$, $f'(x)=2ax+b$.

  \medskip

  \item [$\bullet$] Les fonctions polynômes de degré 3 définies sur $\R$ par : $f(x)=ax^3+bx^2+cx+d$ sont dérivables sur $\R$ et pour tout réel $x$, $f'(x)=3ax^2+2bx+c$.
\end{itemize}
}


~\\
\postit{Propriétés}{
\medskip
Lorsque la fonction dérivée s'annule et change de signe, la fonction $f$ admet un extremum local.
}

\end{document} 