\input mestrucscours.tex

\setmarginsrb{1.7cm}{2.1cm}{1.7cm}{2cm}{0cm}{0cm}{0cm}{-0.5cm}

\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\renewcommand{\arraystretch}{1.5}
\setcounter{chapter}{1}
%\includeonly{chapitre10/fct_troisieme_degre}

\begin{document}
\chapter{Tableaux croisés et probabilités conditionnelles}


\section{Tableaux croisés}

Il est fréquent d'avoir à étudier simultanément deux caractères dans une population de référence.

On utilise pour cela un tableau croisé d'effectifs dans la plupart des cas.

\medskip

\postit{Définition}{
Un tableau croisé d'effectifs est un tableau à double-entrées présentant les valeurs du premier caractère en ligne et celles du second caractère en colonne.

\medskip

\begin{itemize}
  \item [$\bullet$] \`A l'intersection d'une ligne et d'une colonne, le tableau indique le nombre d'individus présentant simultanément la valeur du premier caractère correspondant à cette ligne et la valeur du second caractère correspondant à cette colonne ;

      \medskip

  \item [$\bullet$] On ajoute ensuite une ligne et une colonne \og Total\fg{} indiquant le nombre d'individus présentant chacune des valeurs du caractère. Ce sont les \textbf{effectifs marginaux}.

      \medskip

  \item [$\bullet$] \`A l'intersection de la ligne et de la colonne \og Total\fg{}, on indique l'effectif total, c'est-à-dire le nombre d'individus de la population de référence.
\end{itemize}
}

\medskip

\postit{Définition}{
\begin{itemize}
    \item [$\bullet$] la \textbf{fréquence marginale} de la valeur d'un caractère est la proportion des individus présentant cette valeur dans la population de référence ;

        \medskip

    \item [$\bullet$] \textbf{la fréquence conditionnelle} de la valeur $a$ d'un caractère par rapport à la valeur $b$ du second caractère est la proportion des individus présentant également la valeur $a$ parmi tous ceux présentant la valeur $b$.
\end{itemize}
}




\section{Quelques rappels de probabilités}

\subsection{Vocabulaire}

\postit{Définition}{On appelle\textbf{ expérience aléatoire} une expérience renouvelable dont on ne peut pas connaître à l'avance le résultat obtenu.
Ce résultat n'est pas toujours le même à chaque tentative.

L'\textbf{univers} associé à une expérience aléatoire est l'ensemble de tous les résultats possibles, appelés également éventualités ou issues. Il est noté généralement $\Omega$.
}

\vspace{-0.2cm}

\subsection{Évènement contraire}


\postit{Définition}{
On appelle \textbf{évènement} A tout sous-ensemble de l'univers $\Omega$, c'est à dire un ensemble constitué de \textbf{certains} éléments de $\Omega$. On dit également que l'évènement A est réalisé par ces issues.

\medskip

Le nombre d'issues qui réalisent l'évènement A est appelé \textbf{cardinal} de A.
Il est noté $\text{Card}({\text{A}})$.

\medskip

L'\textbf{évènement contraire} de A, noté $\overline{\text{A}}$, est constitué de toutes les issues de $\Omega$ n'appartenant pas à  A.
}

\medskip

\postit{Propriété}{
Soit $p$ une loi de probabilité sur $\Omega$.
On a alors :
$p(\overline{\text{A}})=1-p(\text{A})$.
}

\vspace{-0.2cm}

\subsection{Intersection et réunion de deux évènements}

\begin{minipage}{8.6cm}
  \postit{Définition}{Soit $\Omega$ un univers associé à une expérience aléatoire, A et B deux événements de $\Omega$.

L'événement constitué des éventualités appartenant à A \textbf{et} à B est noté $\text{A}\cap \text{B}$. (on lit \og A inter B\fg{})
}
\end{minipage}
\begin{minipage}{8.6cm}


\begin{center}
\def\xmin{-1.6} \def\xmax{4.6} \def\ymin{-1.6} \def\ymax{1.6}
\psset{xunit=1cm,yunit=0.75cm}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)

\psclip{%
\pscustom[linestyle=none]{%
\psellipse(.5,0)(1.5,1)}
\pscustom[linestyle=none]{%
\psellipse(2.5,0)(1.5,1)}}
\psframe*[linecolor=lightgray](\xmin,\ymin)(\xmax,\ymax)
\endpsclip

\psellipse(.5,0)(1.5,1)
\psellipse(2.5,0)(1.5,1)
\rput(-1,1){$A$}
\rput(4,1){$B$}
\psdot[dotstyle=x](1.2,0.2)
\uput[dr](1.2,0.2){$e$}

\end{pspicture*}
\end{center}

Ainsi $e \in A \cap B$ signifie $e \in A$ \textbf{et} $e \in B$.

 Lorsque $A \cap B = \emptyset$, on dit que les ensembles $A$ et $B$ sont disjoints.\\
\end{minipage}

\medskip

\textbf{Remarque :}
Deux évènements A et B sont dits incompatibles lorsque $\text{A}\cap\text{B}=\varnothing$.

\medskip

\begin{minipage}{8.6cm}

\postit{Définition}{Soit $\Omega$ un univers lié à une expérience aléatoire, A et B deux événements de $\Omega$.

L'événement constitué des éventualités appartenant à A \textbf{ou} à B est noté $\text{A}\cup \text{B}$. (on lit \og A union B \fg)
}
\end{minipage}
\begin{minipage}{8.6cm}

\begin{center}
\def\xmin{-1.6} \def\xmax{4.6} \def\ymin{-1.6} \def\ymax{1.6}
\psset{xunit=1cm,yunit=0.75cm}
\begin{pspicture*}(\xmin,\ymin)(\xmax,\ymax)

\psclip{%
\pscustom[linestyle=none]{%
\psellipse(.5,0)(1.5,1)}}
\psframe*[linecolor=lightgray](\xmin,\ymin)(\xmax,\ymax)
\endpsclip

\psclip{%
\pscustom[linestyle=none]{%
\psellipse(2.5,0)(1.5,1)}}
\psframe*[linecolor=lightgray](\xmin,\ymin)(\xmax,\ymax)
\endpsclip

\psellipse(.5,0)(1.5,1)
\psellipse(2.5,0)(1.5,1)
\rput(-1,1){$A$}
\rput(4,1){$B$}
\psdot[dotstyle=x](0,0.2)
\uput[dr](0,0.2){$e$}

\end{pspicture*}
\end{center}
Ainsi $e \in A \cup B$ signifie $e \in A$ \textbf{ou} $e \in B$.
\end{minipage}

\medskip

\textbf{Remarque:}
L'intersection des évènements A et B est incluse dans la réunion de A et B.

\medskip

\postit{Propriété}{Soit A et B deux événements de $\Omega$ et $p$ une loi de probabilité sur $\Omega$.

\medskip

Alors on a :
$p(\text{A}\cup \text{B})=p(\text{A})+p(\text{B}) - p(\text{A}\cap \text{B})$.

}

\section{Probabilités conditionnelles}

\postit{Définition}{
Soit A et B deux évènements tels que $\text{Card}(\text{A})\neq0$.
La probabilité conditionnelle de B sachant A est la probabilité que l'évènement B soit réalisé sachant que l'évènement A est réalisé. Elle est notée $p_{\text{A}}(\text{B})$ :
\[p_{\text{A}}({\text{B}})=\dfrac{\text{Card}({\text{A}}\cap {\text{B}})}{\text{Card}({\text{A}})}\]
}

\end{document} 