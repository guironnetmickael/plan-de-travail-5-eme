\input mestrucscours.tex

\setmarginsrb{1.7cm}{2.1cm}{1.7cm}{2cm}{0cm}{0cm}{0cm}{-0.5cm}

\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\renewcommand{\arraystretch}{1.5}
\setcounter{chapter}{2}
%\includeonly{chapitre10/fct_troisieme_degre}

\begin{document}
\chapter{Les suites}
\label{chap4}


\section{Généralités sur les suites}

\postit{Définition}{
\medskip
Une suite numérique $u$ est une fonction définie sur $\N$, à valeurs dans $\R$ :
\begin{center}
$u~:~\N\longrightarrow \R$\\
\hspace{2.2cm}$n\longmapsto u(n)~\text{ ou }~u_n$
\end{center}
On note cette suite $(u_n)$ ou $(u_n)_{n\in\N}$ ou plus simplement $u$.
 }

\medskip

{\bf Remarques :}

\begin{itemize}
  \item [$\bullet$] L'ensemble des entiers naturels est noté $\N$.
  \item [$\bullet$] Une suite est une liste de nombres qui se poursuit : $u_0$, $u_1$, $u_2$, $\ldots$
  \item [$\bullet$] $u_{1,5}$,   $u_{-2}$ n'existent pas ! En revanche $u_0=-2,5$ est tout à fait possible.
  \item [$\bullet$] $u_{n+1}$ est le terme qui suit $u_{n}$.
\end{itemize}

\subsection{Modes de génération d'une suite}

Une suite peut être définie de deux manières différentes :

\medskip

\postit{Définition}{
Une suite $u$ est définie de manière explicite lorsque l'on peut exprimer le terme général $u_n$ en fonction de son indice $n$.
}

\medskip

\textbf{Remarque:}
On peut calculer directement n'importe quel terme $u_n$ de la suite en remplaçant $n$ par la valeur souhaitée.


\postit{Définition}{
Une suite $u$ est définie par récurrence quand elle est définie par la donnée :
\begin{itemize}
  \item [$\bullet$] de son terme initial, généralement $u_0$ ;
  \item [$\bullet$] d'une relation qui permet de calculer à partir de chaque terme le terme suivant.

Cette relation est appelée {\bf relation de récurrence}.
\end{itemize}
}

\medskip

\textbf{Remarque:}
Pour ce type de suite, on ne peut pas calculer directement n'importe quel terme.

En effet, pour déterminer $u_4$, on a besoin de $u_3$ et pour déterminer $u_3$, on a besoin de $u_2$, et ainsi de suite de proche en proche.





\subsection{Représentation graphique}


\postit{Définition}{
Dans un repère $\left(\text{O}~;~\vec{\imath}~,\vec{\jmath}~\right)$, la représentation graphique d'une suite $u$ est l'ensemble des points $M_n$
de coordonnées $(n\,;\,u_n)$.
}

\medskip

\textbf{Remarque :}
Contrairement à une fonction, la représentation graphique d'une
suite n'est pas une courbe mais un nuage de points.



\section{Sens de variation d'une suite}

\postit{Définition}{
\begin{itemize}
  \item [$\bullet$] Une suite $u$ est croissante si pour tout entier
naturel $n$: $u_n\leq u_{n+1}$ ;
  \item [$\bullet$] Une suite $u$ est décroissante si pour tout entier
naturel $n$: $u_n\geq u_{n+1}$ ;
  \item [$\bullet$] Une suite $u$ est constante si pour tout entier naturel $n$: $u_{n+1}=u_n$ ;
  \item [$\bullet$] Une suite $u$ est monotone si elle est croissante ou
décroissante.
\end{itemize}
}

\medskip

\textbf{Remarque:}
Toutes les suites ne sont pas croissantes ou décroissantes.

Par exemple, la suite $(u_n)$ définie par $u_n=(-1)^n$.

\medskip

\postit{Méthode}{
Dans la pratique, pour étudier le sens de variation d'une suite $u$, on étudie le signe de la différence $u_{n+1}-u_n$.
}

\section{Suites arithmétiques et géométriques}

\subsection{Suites arithmétiques}

\postit{Définition}{
Une suite est dite \textbf{arithmétique} lorsque chaque terme se déduit du précédent en lui ajoutant un nombre réel constant $r$, appelé raison de la suite. Ainsi, pour tout $n\in\N$ : $u_{n+1}=u_{n}+r$
}

\postit{Propriété}{
Une suite est arithmétique si et seulement sa représentation graphique est un nuage de points alignés.

On parle alors de croissance linéaire.
}


\postit{Méthode}{
Pour prouver qu'une suite $u$ est arithmétique, on montre que, pour tout $n\in\N$, la différence $u_{n+1}-u_{n}$ est une constante, c'est-à-dire indépendante de l'entier $n$.
}

\
\postit{Propriété}{
Soit $u$ une suite arithmétique de raison $r$.

Si $r>0$, $u$ est croissante \qquad{}; \qquad si $r<0$, $u$ est décroissante \qquad{} ; \qquad si $r=0$, $u$ est constante.
}

\subsection{Suites géométriques}

\postit{Définition}{
Une suite est dite \textbf{géométrique} lorsque chaque terme se déduit du précédent en le multipliant par un nombre réel constant $q$, appelé raison de la suite. Ainsi, pour tout $n\in\N$ : $v_{n+1}q\times v_n$.
}

\medskip


\postit{Méthode}{
Pour prouver qu'une suite $v$ est géométrique, on montre que, pour tout $n\in\N$, la quotient $\dfrac{v_{n+1}}{v_n}$ est une constante, c'est-à-dire indépendante de l'entier $n$.
}


\postit{Propriété}{Soit $v$ une suite géométrique de raison $q$ telle que $v_0>0$ :

\begin{itemize}
  \item [$\bullet$] Si $q>1$, $v$ est croissante \qquad{}; \qquad si $0<q<1$, $v$ est décroissante \qquad{} ; \qquad si $q=1$, $v$ est constante.

      \medskip

  \item [$\bullet$] Le nuage de points représentant la suite $v$ suit une croissance exponentielle.
\end{itemize}




}

\end{document} 