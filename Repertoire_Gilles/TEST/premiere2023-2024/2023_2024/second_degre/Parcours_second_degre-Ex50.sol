\begin{enumerate}
\item $S=\{-7+4\sqrt{2};-7-4\sqrt{2} \}$
\item $S=\{1+\sqrt{2};1-\sqrt{2}\}$
\item $S=\left\{\dfrac{-1+\sqrt{21}}{2};\dfrac{-1-\sqrt{21}}{2}\right\}$
\item $S=\left\{\dfrac{-1+\sqrt{221}}{10};\dfrac{-1-\sqrt{221}}{10}\right\}$
\end{enumerate}
