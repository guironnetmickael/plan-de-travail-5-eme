\begin{enumerate}
  \item
  \begin{enumerate}
  \item $1400$ €.
  \item 90 cartons.
  \end{enumerate}
  \item $R(x)=30x$
  \item
  \begin{enumerate}
    \item
    $B(x)=R(x)-f(x)=-0,25x^2+30x-500$ (attention n'oubliez pas les parenthèses autour de $f(x)$).
    \item Développez $-0,25(x-100)(x-20)$.
  \end{enumerate}
\item Tableau de signes : \\
\begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=2]{$x$/0.5,\footnotesize $f(x)  $/0.5}{$0$,$20$,$100$,$160$}
 \tkzTabLine{,+,z,-,z,+,}
 \end{tikzpicture}~\\
 Tableau de variations : \\
  \begin{tikzpicture}[scale=0.8]
  \tkzTabInit{$x$/1,$f(x)$/2}{$0$,$60$,$160$}
    \tkzTabVar{-/$-500$,+/$400$,-/$-2100$}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}

  \item Quel nombre de cartons doit vendre cet artisan si il veut réaliser un bénéfice positif ?
    \item Quel est le nombre de cartons à vendre pour que son bénéfice soit maximal?
    Calculer alors ce bénéfice.
\end{enumerate}
