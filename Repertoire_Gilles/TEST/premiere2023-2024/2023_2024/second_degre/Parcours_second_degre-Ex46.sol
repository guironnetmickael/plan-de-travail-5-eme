\begin{enumerate}
  \item  $S(1;4)$, $x=1$
  \item Racines : $-1$ et $3$.
  \item $f(0)=3$.
  \item Tableau de signes :\\
  \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=1]{$x$/0.5,\footnotesize $f(x)  $/0.5}{$-\infty$,$-1$,$3$,$+\infty$}
 \tkzTabLine{,+,z,-,z,+,}
 \end{tikzpicture}
  ~\\
  Tableau de variations :\\
  \begin{tikzpicture}[scale=0.8]
  \tkzTabInit{$x$/1,$f(x)$/2}{$-\infty$,$1$,$+\infty$}
    \tkzTabVar{-/,+/$4$,-/$~$}
    %\tkzTabIma{1}{3}{2}{0}
\end{tikzpicture}
  \item $f(x)=-(x+1)(x-3)$ pour la forme factorisée et $f(x)=-x^2+2x+3$ pour la forme factorisée.
  \end{enumerate}
