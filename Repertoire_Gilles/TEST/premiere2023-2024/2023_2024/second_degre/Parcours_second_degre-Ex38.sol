\begin{enumerate}
\item \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=1]{$x$/0.5,\footnotesize $f(x)  $/0.5}{$-\infty$,$1$,$+\infty$}
 \tkzTabLine{,-,d,-,}
 \end{tikzpicture}
\item \begin{tikzpicture}
  \tkzTabInit[lgt=1,espcl=1]{$x$/0.5,\footnotesize $g(x)  $/0.5}{$-\infty$,$-3$,$-2$,$-1$,$+\infty$}
 \tkzTabLine{,+,d,+,z,-,z,+,}
 \end{tikzpicture}
 \end{enumerate}
